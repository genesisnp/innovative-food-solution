$('.form__input').blur(function() {

    if ($(this).val()) {
        $(this).closest( '.form__wrapper' ).addClass('form--filled');
    } else {
        $(this).closest( '.form__wrapper' ).removeClass('form--filled');
    }	
});

$("#form-contact").submit(function(e) {
    e.preventDefault();
}).validate({
	rules: {
		"nombres": {required: true, minlength: 3},
		"empresa": {required: true},
		"email": {required: true, email: true},
		"telefono": {required: true, minlength: 9, number: true},
		"mensaje": {required: true, minlength: 3},
		"acepta": {required: true}
	},messages: {
         "acepta": {required: "requerido"}
	},submitHandler: function (form) {
		
			var formx = $(form);
			var url = formx.attr('action');
		   // var datos = formx.serializeArray();
			var form_data = new FormData(form); 
			console.log(form_data);
			$.ajax({
				type: "post",
				url: url,
				data: form_data,
				processData : false,
				contentType: false,
				cache: false,
				beforeSend: function () {
//                        $.fancybox.open({
//                            src: '#alerta_popup_1',
//                            type: 'inline',
//                            modal: true,
//                            opts: {
//                                modal: true
//                            }
//                        });
			
				}
			}).done(function (msg) {
				modal.style.display = 'flex';				

			});
	}

});
	
/*$('#btn-send-form').on('click', function(){
   
})*/
