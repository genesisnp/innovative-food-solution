<?php

class Contenido extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getContenido($pagina = 0) {
        $this->db->where(array(
            'idpagina' => $pagina,
        ));
        $this->db->order_by('orden', 'ASC');

        $result = $this->db->get('contenido');

        return $this->salida($result->result_array());
    }
    
    public function salida($data = array()) {
        $salida = array();

        foreach ($data as $key => $value) {
            switch ($value['tipo']) {
                case 'data':
                    $valor = json_decode($value['valor'], TRUE);
                    break;
                default:
                    $valor = $value['valor'];
                    break;
            }

            $salida[$value['nombre']] = $valor;
        }

        return $salida;
    }
    
    public function getColumnista($idc){
        $this->db->where('idautor',$idc);
        $jm=$this->db->get('autores');
        return $jm->row_array();
    }
    
    public function getLibros(){
        $this->db->join('sitemap','libros.idsitemap=sitemap.idsitemap');
        $this->db->where('estado',1);
        $this->db->order_by('orden asc');
        $query=$this->db->get('libros');
        return $query->result_array();
    }
    
    public function getLibro($sit=null){
        $this->db->join('sitemap','libros.idsitemap=sitemap.idsitemap');
        $this->db->where('sitemap.url',$sit);
        $query=$this->db->get('libros');
        return $query->row_array();
    }
    
    public function getLibrojm($idl=0){
        $this->db->join('sitemap','libros.idsitemap=sitemap.idsitemap');
        $this->db->where('libros.idlibro',$idl);
        $query=$this->db->get('libros');
        return $query->row_array();
    }
    
    public function getArticulosjm($idc=0){
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");
        $this->db->select('blogs.*,categorias.categoria,sitemap.*,concat(autores.nombres," ",autores.apellidos) as datautor,autores.foto, sys_users.alias');
        $this->db->join('categorias','blogs.idcategoria=categorias.idcategoria');
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->join('autores','blogs.idautor=autores.idautor','LEFT');
        $this->db->join('sys_users','blogs.iduser=sys_users.iduser','LEFT');
        
        $this->db->where(array(
            'blogs.estado'=>1,
            'blogs.idcategoria'=>$idc,
            'blogs.fecha <='=>$fechaactual
        ));
        
        $this->db->order_by('blogs.fecha desc');
        
        $query=$this->db->get('blogs');
        return $query->result_array();
    }
    
    public function getCategoria($ids=0){
        $this->db->join('sitemap','categorias.idsitemap=sitemap.idsitemap');
        $this->db->where('categorias.idsitemap',$ids);
        $query=$this->db->get('categorias');
        return $query->row_array();
    }
    
    public function getCategoriaa($ids=0){
        $this->db->join('sitemap','categorias.idsitemap=sitemap.idsitemap');
        $this->db->where('categorias.idcategoria',$ids);
        $query=$this->db->get('categorias');
        return $query->row_array();
    }
    
    public function getCategoriajm($ids=null){
        $this->db->join('sitemap','categorias.idsitemap=sitemap.idsitemap');
        $this->db->where('categorias.categoria',$ids);
        $query=$this->db->get('categorias');
        return $query->row_array();
    }
    
    public function getTipo($id=0){
        $this->db->where('idpagina',$id);
        $query=$this->db->get('tipo_categoria');
        return $query->row_array();
    }
    
    public function getDistritos(){
        $this->db->where('activo',1);
        $this->db->order_by('distrito ASC');
        $query=$this->db->get('distritos');
        return $query->result();
    }
    
    public function getArticulos($idp=0,$limit = NULL, $offset = NULL){
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");
        $this->db->select('blogs.*,categorias.categoria,concat(autores.nombres," ",autores.apellidos) as datautor,sitemap.*,autores.foto, sys_users.alias');
        $this->db->join('categorias','blogs.idcategoria=categorias.idcategoria');
        $this->db->join('paginas','categorias.idsitemap=paginas.idsitemap');
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->join('autores','blogs.idautor=autores.idautor','LEFT');
        $this->db->join('sys_users','blogs.iduser=sys_users.iduser','LEFT');
        $this->db->where('paginas.idpagina',$idp);
        $this->db->where('blogs.estado',1);
        $this->db->where('blogs.fecha<=',$fechaactual);
        
        if( $limit){
          $this->db->limit($limit,$offset);  
        }
        
        $this->db->order_by('blogs.fecha desc');
        $query=$this->db->get('blogs');
        //echo $this->db->last_query();    
        return $query->result_array();
    }
    
    public function getArticulo($id=0){
        $this->db->select('blogs.*,categorias.categoria,categorias.frase,categorias.color,sitemap.*,concat(autores.nombres," ",autores.apellidos) as datautor,autores.ocupacion,autores.foto');
        $this->db->join('categorias','blogs.idcategoria=categorias.idcategoria');
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->join('autores','blogs.idautor=autores.idautor','LEFT');
//        $this->db->join('paginas','categorias.idpagina=paginas.idpagina');
        $this->db->where('blogs.idblog',$id);
        $query=$this->db->get('blogs');
        return $query->row_array();
    }
    
    public function getArticulosit($id=null){
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
//        $this->db->join('paginas','categorias.idpagina=paginas.idpagina');
        $this->db->where('sitemap.url',$id);
        $query=$this->db->get('blogs');
        return $query->row_array();
    }
    
    public function ultimosArticulos($id=0,$idc=0){
        $result_array = array();
        $categorias = array();
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");
        $this->db->select('blogs.idblog, 
                            blogs.titulo, 
                            blogs.resena, 
                            blogs.descripcion, 
                            blogs.video, 
                            blogs.imagen, 
                            blogs.imagen_grande, 
                            blogs.orden, 
                            blogs.estado, 
                            blogs.fecha, 
                            blogs.tiempo_lectura, 
                            blogs.idsitemap, 
                            blogs.idautor, 
                            blogs.idcategoria, 
                            blogs.entrevista,
                            categorias.categoria,
                            s1.url as blog_url,
                            s2.url as categoria_url')
         ->from('blogs');         
        $this->db->join('categorias','blogs.idcategoria=categorias.idcategoria');
        $this->db->join('sitemap as s1','blogs.idsitemap=s1.idsitemap');
        $this->db->join('sitemap as s2','categorias.idsitemap=s2.idsitemap');
        $this->db->where(array(
            'blogs.idblog <>'=>$id,
            'blogs.idcategoria <> '=>$idc,
            'blogs.estado'=>1,
            'blogs.fecha<='=>$fechaactual
        ));
       
        $this->db->order_by('blogs.fecha desc');
        $query=$this->db->get();
        $results = $query->result_array();

        foreach ($results as $key => $result) {
            
            if(!array_key_exists($result['idcategoria'], $categorias)){
                $categorias[$result['idcategoria']] = $result['categoria'];
                array_push($result_array, $result);
            }
            
        }
        return $result_array;
        
    }

    public function otrosArticulos($id=0,$idc=0){
        
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");        
        $this->db->join('categorias','blogs.idcategoria=categorias.idcategoria');
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->where(array(
            'blogs.idblog <>'=>$id,
            'blogs.idcategoria '=>$idc,
            'blogs.estado'=>1,
            'blogs.fecha<='=>$fechaactual
        ));        
        $this->db->order_by('blogs.fecha desc ');
        $query=$this->db->get('blogs');        

        return $query->result_array();
    }
    
    public function getColumnas(){
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->where(array(
            'blogs.idcategoria'=>10,
            'blogs.estado'=>1
        ));
        $this->db->order_by('orden asc');
        $this->db->limit(5,0);  
        $jm=$this->db->get('blogs');
        return $jm->result_array();
    }
    
    public function getColumnasMasVistas(){
        $this->db->select('blogs.*, count(visitas_web.idvisitas_web) as visitas, sitemap.*' );
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->join('visitas_web','blogs.idblog=visitas_web.idblog', 'LEFT');
        $this->db->where(array(
            'blogs.idcategoria'=>10,
            'blogs.estado'=>1
        ));
        $this->db->group_by('idblog'); 
        $this->db->order_by('visitas desc');
        $this->db->limit(5,0);  
        $jm=$this->db->get('blogs');
        return $jm->result_array();
    }
    
    public function getTags($id=0){
        $this->db->join('tags','tags.idtag=blogs_tags.idtag');
        $this->db->where('blogs_tags.idblog',$id);
        $query=$this->db->get('blogs_tags');
        return $query->result_array();
    }
    
    public function getTag($id=0){
        $this->db->where('idtag',$id);
        $query=$this->db->get('tags');
        return $query->row_array();
    }

    public function getArticulosbyTag($id=0){
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");
        $this->db->join('blogs','blogs_tags.idblog=blogs.idblog');
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->where('blogs_tags.idtag',$id);
        $this->db->where('blogs.fecha<=',$fechaactual);
        $this->db->order_by('blogs.fecha desc');
        $query=$this->db->get('blogs_tags');
        return $query->result_array();
    }
    
    public function getArticulosbyCat($id=0,$limit = NULL, $offset = NULL){
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->where('blogs.idcategoria',$id);
        $this->db->where('blogs.estado',1);
        $this->db->where('blogs.fecha<=',$fechaactual);
        
        if( $limit){
          $this->db->limit($limit,$offset);  
        }
        
        $this->db->order_by('blogs.fecha desc');
        $query=$this->db->get('blogs');
        return $query->result_array();
    }
    
    public function getArticulosbyBus($busca=null){
        date_default_timezone_set('America/Bogota');
        $fechaactual=date("Y-m-d H:i:s");
        $this->db->join('sitemap','blogs.idsitemap=sitemap.idsitemap');
        $this->db->join('autores','blogs.idautor=autores.idautor','LEFT');
        if ($busca != NULL) {
            $this->db->like('blogs.titulo',$busca);
            $this->db->or_like('blogs.descripcion',$busca);
            $this->db->or_like('autores.apellidos',$busca);
            $this->db->or_like('autores.nombres',$busca);
        }
        $this->db->where('blogs.fecha<=',$fechaactual);
        $this->db->order_by('blogs.fecha desc');
        $query=$this->db->get('blogs');
        return $query->result_array();
    }
    
    public function getAnuncios($id=0){
        $this->db->select('anuncios.*,dimensiones.dimension,tipos_anuncios.tipo,tipos_anuncios.idtipo,dimensiones.width,dimensiones.height');
        $this->db->join('dimensiones','anuncios.iddimension=dimensiones.iddimension');
        $this->db->join('tipos_anuncios','dimensiones.idtipo=tipos_anuncios.idtipo');
        $this->db->where('anuncios.idblog',$id);
        $this->db->where('anuncios.estado',1);
        $this->db->order_by('anuncios.orden asc');
        $query=$this->db->get('anuncios');
        return $query->result_array();
    }
    
    public function getAnunciospag($id=0){
        $this->db->select('anuncios.*,dimensiones.dimension,dimensiones.width,dimensiones.height');
        $this->db->join('dimensiones','anuncios.iddimension=dimensiones.iddimension');
//        $this->db->join('tipos_anuncios','dimensiones.idtipo=tipos_anuncios.idtipo');
        $this->db->where('anuncios.idcategoria',$id);
        $this->db->where('anuncios.estado',1);
        $this->db->order_by('anuncios.orden asc');
        $query=$this->db->get('anuncios');
        return $query->result_array();
    }
    
    public function getAnunciosaut($id=0){
        $this->db->select('anuncios.*,dimensiones.dimension,tipos_anuncios.tipo,tipos_anuncios.idtipo,dimensiones.width,dimensiones.height');
        $this->db->join('dimensiones','anuncios.iddimension=dimensiones.iddimension');
        $this->db->join('tipos_anuncios','dimensiones.idtipo=tipos_anuncios.idtipo');
        $this->db->where('anuncios.idautor',$id);
        $this->db->where('anuncios.estado',1);
        $this->db->order_by('anuncios.orden asc');
        $query=$this->db->get('anuncios');
        return $query->result_array();
    }
}
