<?php

class Msuscriptores extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getSuscriptores($search = NULL, $length = 0, $start = 0) {
        if ($search != NULL) {
            $this->db->like('suscriptores.nombres',$search);
            $this->db->or_like('suscriptores.email',$search);
        }
        $this->db->order_by('suscriptores.fecha desc');
        $this->db->limit($length, $start);
        $query=$this->db->get('suscriptores');
        return $query->result_array();
    }
     
    public function getTotal($search = NULL) {
        if ($search != NULL) {
            $this->db->like('suscriptores.nombres',$search);
            $this->db->or_like('suscriptores.email',$search);
        }

        return $this->db->count_all_results('suscriptores');
    }
    
    public function getSuscriptoresExcel(){
        $query=$this->db->get('suscriptores');
        return $query->result_array();
    }
}
