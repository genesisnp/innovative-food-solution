<div class="modal-dialog modal-lg" role="document">
  <form id="formCrearEditarcategoria" action="manager/categorias/save" method="POST">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4><strong>Categoría</strong></h4>
            <style>
                .form-group.required .control-label:after {
                        color: #d00;
                        content: "*";
                        position: absolute;
                        margin-left: 8px;
                        top:2px;
                  }
            </style>
        </div>
        <div class="modal-body">
            
                <div class="container-fluid">
                    <div class="row">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="form-group required">
                                        <label class="control-label">Categoría</label>
                                        <input type="text" class="form-control" id="categoria" name="categorias[categoria]" placeholder="Ingrese nombre de categoria" value="<?= isset($categoria['categoria']) ? $categoria['categoria'] : ''; ?>" >
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group required">
                                        <label class="control-label">Orden</label>
                                        <input type="text" class="form-control" id="orden" name="categorias[orden]"  value="<?= isset($categoria['orden']) ? $categoria['orden'] : ''; ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="form-group required">
                                        <label class="control-label">Página</label>
                                        <select class="form-control">
                                            <option value="">JM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group required">
                                        <label for="campo_3-1" class="control-label">Estado</label>
                                        <select name="categorias[estado]" id="estado" class="form-control">
                                            <option value="1" <?= ($categoria['estado'] == 1 ? 'selected' : '') ?>>Activo</option>
                                            <option value="0" <?= ($categoria['estado'] == 0 ? 'selected' : '') ?>>Inactivo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>  
                
          </div>
          <div class="modal-footer">
                <button type="reset" class="btn btn-default btn-flat" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary btn-flat">Guardar</button>
          </div>
        <input type="hidden" id="ids" name="categorias[idcategoria]" value="<?= isset($categoria['idcategoria']) ? $categoria['idcategoria'] : '';?>">
    </div>
  </form>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        Exeperu.tinyInit('<?= $this->config->item('akey'); ?>');
//        Exeperu.validartipo();
        
        $("#formCrearEditarcategoria").submit(function(e){
           e.preventDefault();
           $.ajax({
              url: $(this).attr('action'),
              type:$(this).attr('method'),
              data:$(this).serialize(),
              success:function(response){
                    var jm=JSON.parse(response);
                    
                    if(jm.tipo==1){
                        toastr.success(jm.mensaje,{timeOut:2000});
                        Exeperu.reloadTableCategorias();
                    }else{
                        toastr.error(jm.mensaje,{timeOut:2000});
                        var errores=JSON.parse(jm.errores); 
                        $.each( errores, function( key, value ) {
                            $("#"+value+"").parent().addClass("has-error");
                        });
                        var jmjm=JSON.parse(jm.jm); 
                        $.each( jmjm, function( key, value ) {
                            $("#"+value+"").parent().removeClass();
                        });
                    }
                    
              }
           });
        });
    });
</script>

</div>