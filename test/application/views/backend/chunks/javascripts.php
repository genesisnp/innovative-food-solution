<link type="text/css" href="mgr/js/plugins/DataTables2/datatables.min.css" rel="stylesheet" property="stylesheet">
<link href="mgr/js/plugins/datetimepicker/datetimepicker.min.css" type="text/css" rel="stylesheet" property="stylesheet">
<link href="mgr/js/plugins/colorpicker/bootstrap-colorpicker.min.css" type="text/css" rel="stylesheet" property="stylesheet">

<!-- Bootstrap 3.3.6 -->
<script type="text/javascript" src="mgr/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="mgr/js/app.min.js"></script>
<!-- TinyMCE -->
<script type="text/javascript" src="mgr/js/plugins/tinymce/tinymce.min.js"></script>
<!-- Validate -->
<script type="text/javascript" src="mgr/js/plugins/Validate/jquery.validate.min.js"></script>
<!-- Datatables -->
<script type="text/javascript" src="mgr/js/plugins/DataTables2/datatables.min.js"></script>
<!-- Select2 -->
<script type="text/javascript" src="mgr/js/plugins/select2/select2.min.js"></script>
<!-- Datetimepicker -->
<script type="text/javascript" src="mgr/js/plugins/datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="mgr/js/plugins/datetimepicker/moment-locale-es.js"></script>
<script type="text/javascript" src="mgr/js/plugins/datetimepicker/datetimepicker.min.js"></script>
<!-- colorpicker -->
<script type="text/javascript" src="mgr/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<script type="text/javascript" src="mgr/js/helpers.js"></script>