<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Administrador - Negocios</title>

<base href="<?= $this->config->base_url() ?>">
<link rel="shortcut icon" href="assets/images/logos/favicon.ico">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="mgr/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="mgr/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="mgr/bower_components/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="mgr/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="mgr/dist/css/skins/skin-blue.min.css">
<link href="mgr/js/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<!-- iCheck -->
<link rel="stylesheet" href="mgr/plugins/iCheck/square/blue.css">
<link rel="stylesheet" href="mgr/plugins/colorpicker/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<link rel="shortcut icon" href="mgr/exeperu/imagen/favicon.ico">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


<!-- jQuery 3 -->
<script src="mgr/bower_components/jquery/dist/jquery.min.js"></script>