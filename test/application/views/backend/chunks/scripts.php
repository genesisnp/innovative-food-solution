<link rel="stylesheet" href="mgr/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link href="mgr/plugins/datetimepicker/datetimepicker.min.css" type="text/css" rel="stylesheet" property="stylesheet">

<!-- Bootstrap 3.3.7 -->
<script src="mgr/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="mgr/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="mgr/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="mgr/dist/js/adminlte.min.js"></script>
<!-- iCheck -->
<script src="mgr/plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="mgr/js/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="mgr/js/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="mgr/js/plugins/datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="mgr/plugins/datetimepicker/moment-locale-es.js"></script>
<script type="text/javascript" src="mgr/plugins/datetimepicker/datetimepicker.min.js"></script>
<script type="text/javascript" src="mgr/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!--<script type="text/javascript" src="assets/js/jquery.form.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>-->
<script type="text/javascript" src="mgr/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="mgr/plugins/iCheck/icheck.min.js"></script>

<script>
//    toastr.options = {
//        "closeButton": false,
//        "debug": false,
//        "newestOnTop": false,
//        "progressBar": false,
//        "positionClass": "toast-bottom-right",
//        "preventDuplicates": false,
//        "onclick": null,
//        "showDuration": "300",
//        "hideDuration": "1000",
//        "timeOut": "2000",
//        "extendedTimeOut": "1000",
//        "showEasing": "swing",
//        "hideEasing": "linear",
//        "showMethod": "fadeIn",
//        "hideMethod": "fadeOut"
//    }
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "2000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
</script>
<!-- Helpers Exeperu -->
<script src="mgr/exeperu/js/helpers.js"></script>