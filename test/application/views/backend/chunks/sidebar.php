<aside class="main-sidebar"><?php $uri_array = $this->uri->segment_array(); ?>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="mgr/exeperu/imagen/negociologo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= ucfirst($this->manager['user']['perfil'])?></p>
                <!-- Status -->
                <a href="manager/perfil/editar"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MÓDULOS</li>
            
            <?php foreach($modulos as $mod){
//                $hijos=$this->sistema->getPaginas($mod['idmodulo']);
                $hijos=[];
                foreach($hijos as $hijo){
                    $lishijos[]=$hijo['idpagina'];
                }
                $uri=$this->uri->segment_array();
                if(isset($uri[5])){
                    $urijm=$uri[5];
                }else{
                    $urijm='';
                }
                if(isset($hijos) && !empty($hijos)){?>
                <li class="<?= (in_array($urijm,$lishijos)) ? 'active' : ''?> treeview">
                    <a href="#">
                      <i class="<?= $mod['icono']?>"></i> <span><?= $mod['modulo']?></span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                    <?php foreach($hijos as $hijo){?>
                      <li class="<?= in_array(''.$hijo['idpagina'].'', $this->uri->segment_array()) ? 'active': '' ?>"><a href="manager/edit/0/0/<?= $hijo['idpagina']?>"><i class="fa fa-circle-o"></i><?= $hijo['menutitle']?></a></li>
                    <?php } ?> 
                    </ul>
                </li>
                <?php 
                }else{?>
                    <li class="<?= (in_array($mod['url'],$this->uri->segment_array()) ? 'active' : '')?>">
                        <a href="manager/<?= $mod['url'] ?>">
                            <i class="<?= $mod['icono']?>"></i> <span><?= $mod['modulo']?></span>
                        </a>
                    </li> 
                <?php }
                } ?>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>