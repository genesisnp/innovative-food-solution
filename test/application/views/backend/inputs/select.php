<div class="form-group">
    <label for="<?= $variable['nombre'] ?>">
        <?= $variable['label']; ?> <small style="color: #999;"><?= $variable['descripcion']; ?></small>
    </label>
    <select name="<?= $variable['nombre'] ?>" id="<?= $variable['nombre'] ?>" class="form-control select2" style="width: 100%;">

    </select>
</div>
<script>
    $(document).ready(function () {
//        var obj = {
//            text: 'hola Luis Text',
//            name: 'hola Luis Name',
//        };
//        console.log(obj.text || obj.name);
//
//        $('#<?= $variable['nombre'] ?>').select2({
//            "data": [
//                {
//                    "id": 1,
//                    "text": "Option 1"
//                },
//                {
//                    "id": 2,
//                    "text": "Option 2",
//                    "selected": true
//                },
//                {
//                    "id": 3,
//                    "text": "Option 3",
//                    "disabled": true
//                }
//            ],
//            "data": [
//                {
//                    "text": "Group 1",
//                    "children": [
//                        {
//                            "id": 1,
//                            "text": "Option 1.1"
//                        },
//                        {
//                            "id": 2,
//                            "text": "Option 1.2"
//                        },
//                        {
//                            "id": 2.1,
//                            "text": "Group 1.3",
//                            "children": [
//                                {
//                                    "id": 1,
//                                    "text": "Option 1.3.1"
//                                },
//                                {
//                                    "id": 2,
//                                    "text": "Option 1.3.2"
//                                }
//                            ]
//                        },
//                    ]
//                },
//                {
//                    "text": "Group 2",
//                    "children": [
//                        {
//                            "id": 3,
//                            "text": "Option 2.1",
//                            "selected": true
//                        },
//                        {
//                            "id": 4,
//                            "text": "Option 2.2"
//                        }
//                    ]
//                }
//            ],
//            "paginate": {
//                "more": true
//            }
//        });
//        $('#<?= $variable['nombre'] ?>').select2({
//            ajax: {
//                url: 'https://api.github.com/search/repositories',
//                dataType: 'json'
//            }
//        });
        $('#<?= $variable['nombre'] ?>').select2(<?= $variable['entrada'] ?>);

    });
</script>