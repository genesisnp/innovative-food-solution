<div class="form-group">
    <label for="<?= $variable['nombre'] ?>">
        <?= $variable['label']; ?> <small style="color: #999;"><?= $variable['descripcion']; ?></small>
    </label>
    <select name="<?= $variable['nombre'] ?>" id="<?= $variable['nombre'] ?>" class="form-control select2" style="width: 100%;">
        <option value="0">Selecciona categoria...</option>

    </select>
</div>
<script>
    $(document).ready(function () {
        listarcategorias();
        verificarid();
        function listarcategorias(){
            var html='<option value="0">Selecciona evento...</option>';
            $.ajax({
               url:'manager/articulos/listacategorias',
               type:'post',
               data:{},
               success:function(response){
                   var obj=JSON.parse(response);
                   $.each(obj,function(i,item){
                       if(item.idcategoria == "<?= isset($variable['valor']) && !empty($variable['valor']) ? $variable['valor'] : '' ?>"){
                           var sel="selected";
                        }else{
                            var sel="";
                        }

                        var nombre=item.subtitulo;
                        var html='<option '+sel+' value="'+item.idcategoria+'">'+item.categoria+'</option>';
                        $("#<?= $variable['nombre'] ?>").append(html);
                   });
               }
            });
        }
        
        function verificarid(){
            var idjm = <?= !empty($variable['valor']) ? $variable['valor'] : '0' ?>;
            if(idjm == 10 || idjm ==8 || idjm ==0){
                $("#imagen-item_anuncioleftimage").parent().parent().parent().parent().css('display', 'none');
                $("#imagen-item_anunciobottomimage").parent().parent().parent().parent().css('display', 'none');
                
            }else{
                $("#imagen-item_anuncioleftimage").parent().parent().parent().parent().css('display', 'inline-block');
                $("#imagen-item_anunciobottomimage").parent().parent().parent().parent().css('display', 'inline-block');
            }
        }
        
        $("#<?= $variable['nombre'] ?>").change(function(e){
            e.preventDefault();
            var jm14=$(this).val();
            if(jm14 == 10 || jm14 ==8|| jm14 ==0){
                $("#imagen-item_anuncioleftimage").parent().parent().parent().parent().css('display', 'none');
                $("#imagen-item_anunciobottomimage").parent().parent().parent().parent().css('display', 'none');
                
            }else{
                $("#imagen-item_anuncioleftimage").parent().parent().parent().parent().css('display', 'inline-block');
                $("#imagen-item_anunciobottomimage").parent().parent().parent().parent().css('display', 'inline-block');
            }
            $.ajax({
               url:'manager/categorias/categorianom',
               type:'post',
               data:{'idcategoria':jm14},
               success:function(response){
                        $("#item_nombrecategoria").val(response);
                    }
                   });
        });

    });
</script>