<div class="form-group">
    <label for="file-<?= $variable['variables-name'] ?>"><?= $variable['variables-label']; ?></label>
    <div class="input-group">
        <input type="text" id="file-<?= $variable['variables-name'] ?>" name="<?= $variable['variables-name'] ?>" class="form-control" placeholder="Selecciona tu archivo" value="<?= isset($variable['variables_values-value']) ? $variable['variables_values-value'] : ''; ?>">
        <div class="input-group-btn">
            <button type="button" class="btn btn-default" aria-label="Search" onclick="Exeperu.popupManager('file-<?= $variable['variables-name'] ?>', '', '<?= $this->config->item('akey'); ?>', 2);">
                <span class="fa fa-file-pdf-o"></span>
            </button>
        </div>
    </div>
</div>