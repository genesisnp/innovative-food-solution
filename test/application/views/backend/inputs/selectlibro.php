<div class="form-group">
    <label for="<?= $variable['nombre'] ?>">
        <?= $variable['label']; ?> <small style="color: #999;"><?= $variable['descripcion']; ?></small>
    </label>
    <!--<input type="text" class="jm" name="<!?= $variable['nombreextra']?>" value="<!?= isset($variable['extra']) ? $variable['extra'] : ''?>">-->
    <select name="<?= $variable['nombre'] ?>" id="<?= $variable['nombre'] ?>" class="form-control select2" style="width: 100%;">
        <option value="0">Selecciona libro...</option>

    </select>
</div>
<script>
    $(document).ready(function () {
        listareventos();
        function listareventos(){
            var html='<option value="0">Selecciona evento...</option>';
            $.ajax({
               url:'manager/libros/listalibros',
               type:'post',
               data:{},
               success:function(response){
                   var obj=JSON.parse(response);
                   $.each(obj,function(i,item){
                       if(item.idlibro == "<?= isset($variable['valor']) && !empty($variable['valor']) ? $variable['valor'] : '' ?>"){
                           var sel="selected";
                        }else{
                            var sel="";
                        }
                        $(".jm").val(item.titulo);
                        var html='<option '+sel+' value="'+item.idlibro+'">'+item.titulo+'</option>';
                        $("#<?= $variable['nombre'] ?>").append(html);
                   });
               }
            });
        }
        
        $("#<?= $variable['nombre'] ?>").change(function(e){
            e.preventDefault();
            var jm14=$(this).val();
            $.ajax({
               url:'manager/libros/libronom',
               type:'post',
               data:{'idlibro':jm14},
               success:function(response){
                        $("#item_nombrelibro").val(response);
                    }
                   });
        });

    });
</script>