<?php
    $data =  array(
        'pagetitle' => 'Productos - Industria Alimentaria',
        'meta_description' => 'Amplia cartera de productos para la industria de alimentos',
        'meta_keywords' => 'Acido Cítrico, Acido fosfórico, almidon de maíz, almidon de papa, benzoato de sodio, industria alimentaria, aditivos para alimentos, ingredientes para alimentos, traders de aditivos, trader',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">insumos</h1>
                <span class="subtitle-banner font-titles-md textUppercase">de alta calidad</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'src/includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-ind-alimentaria"></i>
                                <h1 class="title-primary titles-big">industria</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">alimentaria</h2>
                            </div>
                            <div class="img-products content-img-one d-none d-lg-block wow zoomIn">
                                <img src="assets/images/internas/bajas/ind-alimentaria-int1.jpg" alt="">
                            </div>
                            <div class="img-products content-img-two d-none d-lg-block wow zoomIn">
                                <img src="assets/images/internas/bajas/ind-alimentaria-int2.jpg" alt="">
                            </div>
                            <div class="img-products content-img-three d-none d-lg-block wow zoomIn">
                                <img src="assets/images/internas/bajas/ind-alimentaria-int3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">acidulantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Acético</li>
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Fosfórico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Fumárico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Glucónico 50%</li>
                                    <li class="item-subProducts font-titles-md">Ácido Málico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Láctico</li>
                                </ul>
                            </li>
                            <li class="li-accordion"><a class="titles-subProducts titles-big">colorantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Amarillo N° 5</li>
                                    <li class="item-subProducts font-titles-md">Amarillo N° 6</li>
                                    <li class="item-subProducts font-titles-md">Azul N° 1 Azul</li>
                                    <li class="item-subProducts font-titles-md">N° 2 Caramelo</li>
                                    <li class="item-subProducts font-titles-md">Dióxido de Titanio</li>
                                    <li class="item-subProducts font-titles-md">Rojo N° 40 Rojo</li>
                                    <li class="item-subProducts font-titles-md">N° 3 Rojo Ponceau</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">proteínas</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparagina</li>
                                    <li class="item-subProducts font-titles-md">Proteína Aislada de Soya </li>
                                    <li class="item-subProducts font-titles-md">Proteína Texturizada de Soya</li>
                                    <li class="item-subProducts font-titles-md">Proteína Granulada de Soya</li>
                                    <li class="item-subProducts font-titles-md">Proteína para sales de Inyección</li>
                                    <li class="item-subProducts font-titles-md">Proteínas para Emulsiones</li>
                                    <li class="item-subProducts font-titles-md">Proteínas Lácteas</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">diluyentes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Dipropilenglicol</li>
                                    <li class="item-subProducts font-titles-md">Glicerina</li>
                                    <li class="item-subProducts font-titles-md">Propilenglicol</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">enzimas</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Bromalina</li>
                                    <li class="item-subProducts font-titles-md">Extend Shelf Life</li>
                                    <li class="item-subProducts font-titles-md">Bread Improver</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">fibra</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Papa</li>
                                    <li class="item-subProducts font-titles-md">Polidextrosa</li>
                                    <li class="item-subProducts font-titles-md">Trigo</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">álcalis</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Potasa Caústica</li>
                                    <li class="item-subProducts font-titles-md">Soda Caústica</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Fosfatos</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Fosfato Dipotásico</li>
                                    <li class="item-subProducts font-titles-md">Fosfato Monopotásico</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">secuestrante</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">EDTA</li>
                                    <li class="item-subProducts font-titles-md">gluconato de sodio</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">almidones</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Almidón de Maíz</li>
                                    <li class="item-subProducts font-titles-md">Almidón de Papa</li>
                                    <li class="item-subProducts font-titles-md">Almidón de Trigo</li>
                                    <li class="item-subProducts font-titles-md">Almidón de Yuca</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">frutos en polvo</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aguaymanto</li>
                                    <li class="item-subProducts font-titles-md">Blueberry en trozos</li>
                                    <li class="item-subProducts font-titles-md">Camu Camu</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">agentes de levante</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Bicarbonato de Amonio</li>
                                    <li class="item-subProducts font-titles-md">Bicarbonato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Glucono-delta-lactona</li>
                                    <li class="item-subProducts font-titles-md">Pirofosfato Ácido de Sodio</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">almidones modificados</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Maltodextrina de papa</li>
                                    <li class="item-subProducts font-titles-md">Maltodextrina de maíz</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">enmascaradores</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Edulcorantes</li>
                                    <li class="item-subProducts font-titles-md">Soya</li>
                                    <li class="item-subProducts font-titles-md">Vitaminas y Minerales</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">condimentos</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Para Embutidos</li>
                                    <li class="item-subProducts font-titles-md">Para Marinados</li>
                                    <li class="item-subProducts font-titles-md">Para Sales de cura</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">endulcorantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Acesulfame</li>
                                    <li class="item-subProducts font-titles-md">Aspartame</li>
                                    <li class="item-subProducts font-titles-md">Ciclamato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Dextrosa Anhidra</li>
                                    <li class="item-subProducts font-titles-md">Dextrosa Monohidratada</li>
                                    <li class="item-subProducts font-titles-md">Glucosa 37 - 42DE</li>
                                    <li class="item-subProducts font-titles-md">Sacarina Sódica 20-40 mesh</li>
                                    <li class="item-subProducts font-titles-md">Sorbitol 70%</li>
                                    <li class="item-subProducts font-titles-md">Stevia</li>
                                    <li class="item-subProducts font-titles-md">Sucralosa</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">estabilizantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Alginato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Cloruro de Sodio</li>
                                    <li class="item-subProducts font-titles-md">CMC</li>
                                    <li class="item-subProducts font-titles-md">Crémor Tártaro</li>
                                    <li class="item-subProducts font-titles-md">Gelatina</li>
                                    <li class="item-subProducts font-titles-md">Gelatina 230 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatina 260 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatina 280 BLOOM </li>
                                    <li class="item-subProducts font-titles-md">Goma Arábiga</li>
                                    <li class="item-subProducts font-titles-md">Goma Guar</li>
                                    <li class="item-subProducts font-titles-md">Goma Xantan</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">antioxidantes y conservantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Ascórbico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Sórbico</li>
                                    <li class="item-subProducts font-titles-md">Ascobato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Benzoato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Benzoato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">BHT</li>
                                    <li class="item-subProducts font-titles-md">Citrato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Diacetatos</li>
                                    <li class="item-subProducts font-titles-md">Eritorbato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Metabisulﬁto de sodio</li>
                                    <li class="item-subProducts font-titles-md">Natamicina</li>
                                    <li class="item-subProducts font-titles-md">Nitrito de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Propionato de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Sorbato de Potasio</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">sabores y resaltadores de sabor</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Anethol</li>
                                    <li class="item-subProducts font-titles-md">Café</li>
                                    <li class="item-subProducts font-titles-md">Cafeína anhidra</li>
                                    <li class="item-subProducts font-titles-md">Carnes Diversas</li>
                                    <li class="item-subProducts font-titles-md">Cítricos Especias</li>
                                    <li class="item-subProducts font-titles-md">Eucalipto</li>
                                    <li class="item-subProducts font-titles-md">Frutos Azules
                                    <li class="item-subProducts font-titles-md">Frutos Peruanos /Tropical</li>
                                    <li class="item-subProducts font-titles-md">Frutos Rojos/Verdes/Anaranjados</li>
                                    <li class="item-subProducts font-titles-md">Humo</li>
                                    <li class="item-subProducts font-titles-md">Menthol/Menta</li>
                                    <li class="item-subProducts font-titles-md">Quesos variados</li>
                                    <li class="item-subProducts font-titles-md">Te negro/verde</li>
                                    <li class="item-subProducts font-titles-md">Tutti Frutti</li>
                                    <li class="item-subProducts font-titles-md">Vainilla-Vainillina-Ethil Vainillina
                                    </li>
                                    <li class="item-subProducts font-titles-md">Vegetales</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">emulsificantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Lecitina de Soya</li>
                                    <li class="item-subProducts font-titles-md">Mono y Diglicéridos</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">lácteos en polvo</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Leche Descremada</li>
                                    <li class="item-subProducts font-titles-md">Leche Entera</li>
                                    <li class="item-subProducts font-titles-md">Permeato de Suero</li>
                                    <li class="item-subProducts font-titles-md">Suero de Leche</li>
                                    <li class="item-subProducts font-titles-md">Sustituto de leche</li>
                                    <li class="item-subProducts font-titles-md">Sustituto de suero</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">humectante</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">glicerina</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">antiapelmazantes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">fosfato tricálcico</li>
                                    <li class="item-subProducts font-titles-md">sales de silice</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">tensoactivo</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">polisorbato 20</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">lubricante</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">vaselina</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">descolesterolizador</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">his down col</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">acidulantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Acético</li>
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Fosfórico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Fumárico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Glucónico 50%</li>
                                    <li class="item-subProducts font-titles-md">Ácido Málico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Láctico</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">colorantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Amarillo N° 5</li>
                                    <li class="item-subProducts font-titles-md">Amarillo N° 6</li>
                                    <li class="item-subProducts font-titles-md">Azul N° 1 Azul</li>
                                    <li class="item-subProducts font-titles-md">N° 2 Caramelo</li>
                                    <li class="item-subProducts font-titles-md">Dióxido de Titanio</li>
                                    <li class="item-subProducts font-titles-md">Rojo N° 40 Rojo</li>
                                    <li class="item-subProducts font-titles-md">N° 3 Rojo Ponceau</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">proteínas</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparagina</li>
                                    <li class="item-subProducts font-titles-md">Proteína Aislada de Soya </li>
                                    <li class="item-subProducts font-titles-md">Proteína Texturizada de Soya</li>
                                    <li class="item-subProducts font-titles-md">Proteína Granulada de Soya</li>
                                    <li class="item-subProducts font-titles-md">Proteína para sales de Inyección</li>
                                    <li class="item-subProducts font-titles-md">Proteínas para Emulsiones</li>
                                    <li class="item-subProducts font-titles-md">Proteínas Lácteas</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">diluyentes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Dipropilenglicol</li>
                                    <li class="item-subProducts font-titles-md">Glicerina</li>
                                    <li class="item-subProducts font-titles-md">Propilenglicol</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">enzimas</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Bromalina</li>
                                    <li class="item-subProducts font-titles-md">Extend Shelf Life</li>
                                    <li class="item-subProducts font-titles-md">Bread Improver</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">fibra</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Papa</li>
                                    <li class="item-subProducts font-titles-md">Polidextrosa</li>
                                    <li class="item-subProducts font-titles-md">Trigo</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">álcalis</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Potasa Caústica</li>
                                    <li class="item-subProducts font-titles-md">Soda Caústica</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Fosfatos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Fosfato Dipotásico</li>
                                    <li class="item-subProducts font-titles-md">Fosfato Monopotásico</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">secuestrante</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">EDTA</li>
                                    <li class="item-subProducts font-titles-md">gluconato de sodio</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">almidones</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Almidón de Maíz</li>
                                    <li class="item-subProducts font-titles-md">Almidón de Papa</li>
                                    <li class="item-subProducts font-titles-md">Almidón de Trigo</li>
                                    <li class="item-subProducts font-titles-md">Almidón de Yuca</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">frutos en polvo<br>trozos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aguaymanto</li>
                                    <li class="item-subProducts font-titles-md">Blueberry en trozos</li>
                                    <li class="item-subProducts font-titles-md">Camu Camu</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">agentes<br>de levante</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Bicarbonato de Amonio</li>
                                    <li class="item-subProducts font-titles-md">Bicarbonato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Glucono-delta-lactona</li>
                                    <li class="item-subProducts font-titles-md">Pirofosfato Ácido de Sodio</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">almidones<br>modificados</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Maltodextrina de papa</li>
                                    <li class="item-subProducts font-titles-md">Maltodextrina de maíz</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">enmascaradores</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Edulcorantes</li>
                                    <li class="item-subProducts font-titles-md">Soya</li>
                                    <li class="item-subProducts font-titles-md">Vitaminas y Minerales</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">condimentos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Para Embutidos</li>
                                    <li class="item-subProducts font-titles-md">Para Marinados</li>
                                    <li class="item-subProducts font-titles-md">Para Sales de cura</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">endulcorantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Acesulfame</li>
                                    <li class="item-subProducts font-titles-md">Aspartame</li>
                                    <li class="item-subProducts font-titles-md">Ciclamato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Dextrosa Anhidra</li>
                                    <li class="item-subProducts font-titles-md">Dextrosa Monohidratada</li>
                                    <li class="item-subProducts font-titles-md">Glucosa 37 - 42DE</li>
                                    <li class="item-subProducts font-titles-md">Sacarina Sódica 20-40 mesh</li>
                                    <li class="item-subProducts font-titles-md">Sorbitol 70%</li>
                                    <li class="item-subProducts font-titles-md">Stevia</li>
                                    <li class="item-subProducts font-titles-md">Sucralosa</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">estabilizantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Alginato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Cloruro de Sodio</li>
                                    <li class="item-subProducts font-titles-md">CMC</li>
                                    <li class="item-subProducts font-titles-md">Crémor Tártaro</li>
                                    <li class="item-subProducts font-titles-md">Gelatina</li>
                                    <li class="item-subProducts font-titles-md">Gelatina 230 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatina 260 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatina 280 BLOOM </li>
                                    <li class="item-subProducts font-titles-md">Goma Arábiga</li>
                                    <li class="item-subProducts font-titles-md">Goma Guar</li>
                                    <li class="item-subProducts font-titles-md">Goma Xantan</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">antioxidantes y conservantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Ascórbico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Sórbico</li>
                                    <li class="item-subProducts font-titles-md">Ascobato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Benzoato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Benzoato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">BHT</li>
                                    <li class="item-subProducts font-titles-md">Citrato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Diacetatos</li>
                                    <li class="item-subProducts font-titles-md">Eritorbato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Metabisulﬁto de sodio</li>
                                    <li class="item-subProducts font-titles-md">Natamicina</li>
                                    <li class="item-subProducts font-titles-md">Nitrito de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Propionato de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Sorbato de Potasio</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">sabores<br>y resaltadores<br>de sabor</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Anethol</li>
                                    <li class="item-subProducts font-titles-md">Café</li>
                                    <li class="item-subProducts font-titles-md">Cafeína anhidra</li>
                                    <li class="item-subProducts font-titles-md">Carnes Diversas</li>
                                    <li class="item-subProducts font-titles-md">Cítricos Especias</li>
                                    <li class="item-subProducts font-titles-md">Eucalipto</li>
                                    <li class="item-subProducts font-titles-md">Frutos Azules
                                    <li class="item-subProducts font-titles-md">Frutos Peruanos /Tropical</li>
                                    <li class="item-subProducts font-titles-md">Frutos Rojos/Verdes/Anaranjados</li>
                                    <li class="item-subProducts font-titles-md">Humo</li>
                                    <li class="item-subProducts font-titles-md">Menthol/Menta</li>
                                    <li class="item-subProducts font-titles-md">Quesos variados</li>
                                    <li class="item-subProducts font-titles-md">Te negro/verde</li>
                                    <li class="item-subProducts font-titles-md">Tutti Frutti</li>
                                    <li class="item-subProducts font-titles-md">Vainilla-Vainillina-Ethil Vainillina
                                    </li>
                                    <li class="item-subProducts font-titles-md">Vegetales</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">emulsificantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Lecitina de Soya</li>
                                    <li class="item-subProducts font-titles-md">Mono y Diglicéridos</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">lácteos<br>en polvo</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Leche Descremada</li>
                                    <li class="item-subProducts font-titles-md">Leche Entera</li>
                                    <li class="item-subProducts font-titles-md">Permeato de Suero</li>
                                    <li class="item-subProducts font-titles-md">Suero de Leche</li>
                                    <li class="item-subProducts font-titles-md">Sustituto de leche</li>
                                    <li class="item-subProducts font-titles-md">Sustituto de suero</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">humectante</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">glicerina</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">antiapelmazantes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">fosfato tricálcico</li>
                                    <li class="item-subProducts font-titles-md">sales de silice</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">tensoactivo</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">polisorbato 20</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">lubricante</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">vaselina</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">descolesterolizador</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">his down col</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="assets/images/icons/forma_esquina.svg" alt="">
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/accordion.js"></script>
</body>

</html>