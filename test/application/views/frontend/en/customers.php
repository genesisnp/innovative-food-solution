<?php
    $data =  array(
        'pagetitle' => 'Clientes',
        'meta_description' => 'Creamos Relaciones y alianzas estratégicas con nuestros clientes',
        'meta_keywords' => '',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'includes/header.php'
?>
    <main class="main-customers">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center  wrapper-ancla">
            <img class="bg-shadow" src="../assets/images/sombra.png" alt="">
            <div class="content-img-banner vh">
                <img src="../assets/images/banner/bajas/clientes.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big">Global</h1>
                <span class="subtitle-banner font-titles-md">customers</span>
            </div>
            <a href="#inf-customrs" data-ancla="inf-customrs" class="content-ancla d-none d-lg-block">
                <h1 class="h1-text-Rotate font-internas color-white">More information</h1>
                <i class="color-white icon-flecha"></i>
            </a>
        </section>
        <!--DESCRIPTION CLIENTES-->
        <section class="section-sct-description overflow-h" id="inf-customrs">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-7 col-xl-5">
                        <div class="row">
                            <div class="col-12 col-lg-11">
                                <div class="row justify-content-center">
                                    <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                        <i class="icon-descr icon-clientes"></i>
                                        <h1 class="titles-big color-secondary">Comittment and</h1>
                                        <span class="subtitle-internas font-titles-md">Y
                                            RESPONSABILITY</span>
                                    </div>
                                    <div class="col-12 col-lg-11 wow fadeInLeft">
                                        <p class="font-internas">Our Customers are located in the 5 Continents, we have developed together an strong 
                                        commercial relationship based in the quality of our products, reliability and strength of our supply chain.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-11 col-md-9 col-lg-5 col-xl-7">
                        <div class="content-img wow zoomIn">
                            <img src="../assets/images/internas/bajas/clientes-int.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="../assets/images/icons/forma.svg" alt="">
        </section>
    </main>
    <?php
        include 'includes/footer.php'
    ?>
</body>

</html>