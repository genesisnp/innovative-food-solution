<section class="sct-navbar-products nb-serv">
    <div class="wow zoomIn wrapper-navP <?= in_array('we-guarantee-the-delivery', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="we-guarantee-the-delivery" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-despacho"></i>
                <h1 class="font-titles-md textUppercase">WE GUARANTEE<br>THE DELIVERY</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('specialised-technical-consulting', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="specialised-technical-consulting" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-asesoria"></i>
                <h1 class="font-titles-md textUppercase">SPECIALISED TECHNICAL<br>CONSULTING</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('quality-of-our-products', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="quality-of-our-products" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-calidad"></i>
                <h1 class="font-titles-md textUppercase">QUALITY<br>OF OUR<br>PRODUCTS</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('favorable-payment-conditions', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="favorable-payment-conditions" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cond-de-pago"></i>
                <h1 class="font-titles-md textUppercase">FAVORABLE<br>PAYMENT<br>CONDITIONS</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('current-market-information', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="current-market-information" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-info-de-mercad"></i>
                <h1 class="font-titles-md textUppercase">CURRENT MARKET<br>INFORMATION</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('tendency-information', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="tendency-information" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-info-de-tend"></i>
                <h1 class="font-titles-md textUppercase">TENDENCY<br>INFORMATION</h1>
            </div>
        </a>
    </div>
</section>