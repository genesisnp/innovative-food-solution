<section class="sct-navbar-products">
    <div class="wow zoomIn wrapper-navP <?= in_array('food-industry', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="food-industry" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-ind-alimentaria"></i>
                <h1 class="font-titles-md textUppercase">FOOD<br>INDUSTRY</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('nutrition-and-healthy', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="nutrition-and-healthy" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-nutricion"></i>
                <h1 class="font-titles-md textUppercase">NUTRITION<br>AND HEALTHY</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('personal-care', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="personal-care" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cuidado-personal"></i>
                <h1 class="font-titles-md textUppercase">PERSONAL<br>CARE</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('home-care-and-others', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="home-care-and-others" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cuidado-hogar"></i>
                <h1 class="font-titles-md textUppercase">HOME CARE<br>AND OTHERS</h1>
            </div>
        </a>
    </div>
</section>