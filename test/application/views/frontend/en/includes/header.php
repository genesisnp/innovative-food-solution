<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INNOVATIVE FOOD SOLUTIONS - <?php echo $data['pagetitle'] ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/logo-ifs.ico" />
    <meta name="description" content="<?php echo $data['meta_description'] ?>">
    <meta name="keywords" content="<?php echo $data['meta_keywords'] ?>">
    <meta name="author" content="EXE MARKETING DIGITAL INTEGRADO" />
    <meta name="copyright" content="INNOVATIVE FOOD SOLUTIONS" />
    <meta name="language" content="EN" />
    <meta name="robots" content="index,follow" />
	<base href="<?= base_url() ?>en/" >
    <link rel="stylesheet" href="../assets/css/app.css">
	
</head>

<body>
    <?php 
        $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));    
    ?>
    <header class="header">
        <img class="bg-logo" src="../assets/images/bg-logo.png" alt="bg/logo">
        <div class="logo">
            <a href="">
                <img class="img-logo" src="../assets/images/logo-innovative.png" alt="logo/innovative">
            </a>
        </div>
        <i id="button-menu" class="icon-hamburger"></i>

        <div class="inner-header container">
            <div class="content-menu floatR">
                <div class="phonesHeader textR">
                    <i class="icon-phone-h"></i>
                    <a class="aPhone aPhoneOne font-titles-md" href="tel:+34915799360"><span>+34 648 887 963</span></a>
                    <a class="aPhone font-titles-md" href="tel:+34915789360"><span>+34 915 789 360</span></a>
                    <a href="../inicio" class="aPhone lenguage font-titles-md color-white"><span>ES</span></a>
                </div>
                <nav class="navbar">
                    <ul class="navbarList floatR">
                        <li class="navbarItem  <?= in_array('company', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="company">company</a></li>
                        <li class="navbarItem  <?= (in_array('food-industry', $uriSegments )
                                                    or in_array('nutrition-and-healthy', $uriSegments )
                                                    or in_array('personal-care', $uriSegments )
                                                    or in_array('home-care-and-others', $uriSegments ))? 'active' : ''; ?>">
                            <span class="navbarLink font-titles-md">products</span>
                            <div class="submenu">
                                <div class="wrapper-submenu">
                                    <ul class="tabs floatL container">
                                        <li class="tab-item  <?= in_array('food-industry', $uriSegments ) ? 'active' : ''; ?>"><a class="tab-link textUppercase font-titles-md"
                                                href="food-industry" data-name="#tab1">FOOD INDUSTRY</a>
                                        </li>
                                        <li class="tab-item  <?= in_array('personal-care', $uriSegments ) ? 'active' : ''; ?>">
                                        <a class="tab-link textUppercase font-titles-md" href="personal-care"
                                                data-name="#tab2">PERSONAL CARE</a></li>
                                        <li class="tab-item  <?= in_array('nutrition-and-healthy', $uriSegments ) ? 'active' : ''; ?>">
                                        <a class="tab-link textUppercase font-titles-md" href="nutrition-and-healthy"
                                                data-name="#tab3">NUTRITION AND HEALTHY</a></li>
                                        <li class="tab-item  <?= in_array('home-care-and-others', $uriSegments ) ? 'active' : ''; ?>">
                                        <a class="tab-link textUppercase font-titles-md" href="home-care-and-others"
                                                data-name="#tab4">HOME CARE AND OTHERS</a></li>
                                    </ul>

                                    <div class="secciones floatL">
                                        <div class="tab-img" id="tab1">
                                            <img class="wow fadeIn" src="../assets/images/internas/bajas/ind-alimentaria-int1.jpg" alt="">
                                        </div>
                                        <div class="tab-img" id="tab2">
                                            <img class="wow fadeIn" src="../assets/images/internas/bajas/cuidado-personal1.jpg" alt="">
                                        </div>
                                        <div class="tab-img" id="tab3">
                                            <img class="wow fadeIn" src="../assets/images/internas/bajas/nutricion-1.jpg" alt="">
                                        </div>
                                        <div class="tab-img" id="tab4">
                                            <img class="wow fadeIn" src="../assets/images/internas/bajas/limpieza.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="navbarItem  <?= (in_array('we-guarantee-the-delivery', $uriSegments ) 
                                                    or in_array('specialised-technical-consulting', $uriSegments )
                                                    or in_array('quality-of-our-products', $uriSegments )
                                                    or in_array('favorable-payment-conditions', $uriSegments )
                                                    or in_array('current-market-information', $uriSegments )
                                                    or in_array('tendency-information', $uriSegments )) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="we-guarantee-the-delivery">services</a></li>
                        <li class="navbarItem  <?= in_array('customers', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="customers">customers</a>
                        </li>
                        <li class="navbarItem  <?= in_array('contact', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="contact">contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>