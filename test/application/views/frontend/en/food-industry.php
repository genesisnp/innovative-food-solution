<?php 
    $data =  array(
        'pagetitle' => 'Productos - Industria Alimentaria',
        'meta_description' => 'Amplia cartera de productos para la industria de alimentos',
        'meta_keywords' => 'Acido Cítrico, Acido fosfórico, almidon de maíz, almidon de papa, benzoato de sodio, industria alimentaria, aditivos para alimentos, ingredientes para alimentos, traders de aditivos, trader',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="../assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="../assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">high</h1>
                <span class="subtitle-banner font-titles-md textUppercase">quality supplies</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-ind-alimentaria"></i>
                                <h1 class="title-primary titles-big">food</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">INDUSTRY</h2>
                            </div>
                            <div class="img-products content-img-one d-none d-lg-block wow zoomIn">
                                <img src="../assets/images/internas/bajas/ind-alimentaria-int1.jpg" alt="">
                            </div>
                            <div class="img-products content-img-two d-none d-lg-block wow zoomIn">
                                <img src="../assets/images/internas/bajas/ind-alimentaria-int2.jpg" alt="">
                            </div>
                            <div class="img-products content-img-three d-none d-lg-block wow zoomIn">
                                <img src="../assets/images/internas/bajas/ind-alimentaria-int3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">Acidulants</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Acetic Acid</li>
                                    <li class="item-subProducts font-titles-md">Citric acid</li>
                                    <li class="item-subProducts font-titles-md">Phosphoric acid</li>
                                    <li class="item-subProducts font-titles-md">Fumaric Acid</li>
                                    <li class="item-subProducts font-titles-md">Gluconic acid 50%</li>
                                    <li class="item-subProducts font-titles-md">Malic acid</li>
                                    <li class="item-subProducts font-titles-md">lactic acid</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Colors</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Yellow N° 5</li>
                                    <li class="item-subProducts font-titles-md">Yellow N° 6</li>
                                    <li class="item-subProducts font-titles-md">Blue N° 1</li>
                                    <li class="item-subProducts font-titles-md">Red N° 2</li>
                                    <li class="item-subProducts font-titles-md">Titanium Dioxide</li>
                                    <li class="item-subProducts font-titles-md">Red N° 40</li>
                                    <li class="item-subProducts font-titles-md">Red N° 3</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">proteins</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparagine</li>
                                    <li class="item-subProducts font-titles-md">Isolated soy Protein</li>
                                    <li class="item-subProducts font-titles-md">Texturizen soy Protein</li>
                                    <li class="item-subProducts font-titles-md">Granulated soy Protein</li>
                                    <li class="item-subProducts font-titles-md">Protein for injection Salts</li>
                                    <li class="item-subProducts font-titles-md">Protein for emulsion</li>
                                    <li class="item-subProducts font-titles-md">Dairy Protein</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">diluents</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">dipropylene glycol</li>
                                    <li class="item-subProducts font-titles-md">glycerine</li>
                                    <li class="item-subProducts font-titles-md">propylene glycol</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">enzymes</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">bromaline</li>
                                    <li class="item-subProducts font-titles-md">extended shelf life</li>
                                    <li class="item-subProducts font-titles-md">Bread Improver</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">fibers</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">potato</li>
                                    <li class="item-subProducts font-titles-md">polidextrose</li>
                                    <li class="item-subProducts font-titles-md">wheat</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">alcalis</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">potassium hydroxide</li>
                                    <li class="item-subProducts font-titles-md">caustic soda</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">phosphates</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">dipotassium phosphate</li>
                                    <li class="item-subProducts font-titles-md">monopotassium phosphate</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">chelating agents</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">EDTA</li>
                                    <li class="item-subProducts font-titles-md">sodium gluconate</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">starches</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">corn starch</li>
                                    <li class="item-subProducts font-titles-md">potato starch</li>
                                    <li class="item-subProducts font-titles-md">wheat starch</li>
                                    <li class="item-subProducts font-titles-md">tapioca starch</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">fruits in pieces / powder</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aguaymanto</li>
                                    <li class="item-subProducts font-titles-md">blueberry</li>
                                    <li class="item-subProducts font-titles-md">Camu Camu</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">lifting agents</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">ammonium bicarbonate</li>
                                    <li class="item-subProducts font-titles-md">sodium bicarbonate</li>
                                    <li class="item-subProducts font-titles-md">gluconodeltalactone</li>
                                    <li class="item-subProducts font-titles-md">sodium acid pyrophoshate</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">modified starches</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">potato maltodextrine</li>
                                    <li class="item-subProducts font-titles-md">corn maltodextrine</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">maskers</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">sweeteners</li>
                                    <li class="item-subProducts font-titles-md">Soy</li>
                                    <li class="item-subProducts font-titles-md">vitamins and minerals</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">spices</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">for sausages</li>
                                    <li class="item-subProducts font-titles-md">for marinates</li>
                                    <li class="item-subProducts font-titles-md">for curing salts</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">sweeteners</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">acesulfame</li>
                                    <li class="item-subProducts font-titles-md">aspartame</li>
                                    <li class="item-subProducts font-titles-md">sodium cyclamate</li>
                                    <li class="item-subProducts font-titles-md">anhydrous dextrose</li>
                                    <li class="item-subProducts font-titles-md">monohydrate dextrose</li>
                                    <li class="item-subProducts font-titles-md">glucose</li>
                                    <li class="item-subProducts font-titles-md">sodium saccharine</li>
                                    <li class="item-subProducts font-titles-md">sorbitol</li>
                                    <li class="item-subProducts font-titles-md">Stevia</li>
                                    <li class="item-subProducts font-titles-md">sucralose</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">stabilizers</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">sodium alginate</li>
                                    <li class="item-subProducts font-titles-md">sodium chloride</li>
                                    <li class="item-subProducts font-titles-md">CMC</li>
                                    <li class="item-subProducts font-titles-md">cream of tartar</li>
                                    <li class="item-subProducts font-titles-md">Gelatin</li>
                                    <li class="item-subProducts font-titles-md">Gelatin 230 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatin 260 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatin 280 BLOOM </li>
                                    <li class="item-subProducts font-titles-md">arabic gum</li>
                                    <li class="item-subProducts font-titles-md">guar gum</li>
                                    <li class="item-subProducts font-titles-md">xanthan gum</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">antioxidants and preservatives</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">ascorbic acid</li>
                                    <li class="item-subProducts font-titles-md">sorbic acid</li>
                                    <li class="item-subProducts font-titles-md">sodium ascorbate</li>
                                    <li class="item-subProducts font-titles-md">sodium benzoate</li>
                                    <li class="item-subProducts font-titles-md">potassium benzoate</li>
                                    <li class="item-subProducts font-titles-md">BHT</li>
                                    <li class="item-subProducts font-titles-md">sodium citrate</li>
                                    <li class="item-subProducts font-titles-md">diacetates</li>
                                    <li class="item-subProducts font-titles-md">sodium erythorbate</li>
                                    <li class="item-subProducts font-titles-md">potassium lactate</li>
                                    <li class="item-subProducts font-titles-md">Sodium Lactate</li>
                                    <li class="item-subProducts font-titles-md">sodium metabisulphite</li>
                                    <li class="item-subProducts font-titles-md">natamycin</li>
                                    <li class="item-subProducts font-titles-md">sodium nitrite</li>
                                    <li class="item-subProducts font-titles-md">calcium propionate</li>
                                    <li class="item-subProducts font-titles-md">potassium sorbate</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">flavors and enhancers</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Anethol</li>
                                    <li class="item-subProducts font-titles-md">coffee</li>
                                    <li class="item-subProducts font-titles-md">anhydrous caffeine</li>
                                    <li class="item-subProducts font-titles-md">Carnes Diversas</li>
                                    <li class="item-subProducts font-titles-md">Citrus</li>
                                    <li class="item-subProducts font-titles-md">spices</li>
                                    <li class="item-subProducts font-titles-md">eucaliptol</li>
                                    <li class="item-subProducts font-titles-md">blue fruits</li>
                                    <li class="item-subProducts font-titles-md">peruvian / tropical fruits</li>
                                    <li class="item-subProducts font-titles-md">red / green / orange fruits</li>
                                    <li class="item-subProducts font-titles-md">Humo</li>
                                    <li class="item-subProducts font-titles-md">menthol</li>
                                    <li class="item-subProducts font-titles-md">cheese</li>
                                    <li class="item-subProducts font-titles-md">green/black tee</li>
                                    <li class="item-subProducts font-titles-md">tutti frutti</li>
                                    <li class="item-subProducts font-titles-md">vainillina / ethyl vainillina
                                    </li>
                                    <li class="item-subProducts font-titles-md">vegetables</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">emulsifiers</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">soy lecithin</li>
                                    <li class="item-subProducts font-titles-md">mono and diglycerids</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">dairy products</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">skimmed milk</li>
                                    <li class="item-subProducts font-titles-md">full cream milk</li>
                                    <li class="item-subProducts font-titles-md">deproteinized whey</li>
                                    <li class="item-subProducts font-titles-md">whey</li>
                                    <li class="item-subProducts font-titles-md">milk replacer</li>
                                    <li class="item-subProducts font-titles-md">milk replacer</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">moisturizers</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">glycerine</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">anti compacting agents</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">tricalcium phosphate</li>
                                    <li class="item-subProducts font-titles-md">silica satls</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">surfactants</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">polisorbate</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">lubricants</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">vaseline</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">non cholesterol</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">down col</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Acidulants</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Acetic Acid</li>
                                    <li class="item-subProducts font-titles-md">Citric acid</li>
                                    <li class="item-subProducts font-titles-md">Phosphoric acid</li>
                                    <li class="item-subProducts font-titles-md">Fumaric Acid</li>
                                    <li class="item-subProducts font-titles-md">Gluconic acid 50%</li>
                                    <li class="item-subProducts font-titles-md">Malic acid</li>
                                    <li class="item-subProducts font-titles-md">lactic acid</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">colors</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Yellow N° 5</li>
                                    <li class="item-subProducts font-titles-md">Yellow N° 6</li>
                                    <li class="item-subProducts font-titles-md">Blue N° 1</li>
                                    <li class="item-subProducts font-titles-md">Red N° 2</li>
                                    <li class="item-subProducts font-titles-md">Titanium Dioxide</li>
                                    <li class="item-subProducts font-titles-md">Red N° 40</li>
                                    <li class="item-subProducts font-titles-md">Red N° 3</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">proteins</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparagine</li>
                                    <li class="item-subProducts font-titles-md">Isolated soy Protein</li>
                                    <li class="item-subProducts font-titles-md">Texturizen soy Protein</li>
                                    <li class="item-subProducts font-titles-md">Granulated soy Protein</li>
                                    <li class="item-subProducts font-titles-md">Protein for injection Salts</li>
                                    <li class="item-subProducts font-titles-md">Protein for emulsion</li>
                                    <li class="item-subProducts font-titles-md">Dairy Protein</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">diluents</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">dipropylene glycol</li>
                                    <li class="item-subProducts font-titles-md">glycerine</li>
                                    <li class="item-subProducts font-titles-md">propylene glycol</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">enzymes</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">bromaline</li>
                                    <li class="item-subProducts font-titles-md">extended shelf life</li>
                                    <li class="item-subProducts font-titles-md">Bread Improver</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">fibers</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">potato</li>
                                    <li class="item-subProducts font-titles-md">polidextrose</li>
                                    <li class="item-subProducts font-titles-md">wheat</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">alcalis</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">potassium hydroxide</li>
                                    <li class="item-subProducts font-titles-md">caustic soda</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">phosphates</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">dipotassium phosphate</li>
                                    <li class="item-subProducts font-titles-md">monopotassium phosphate</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">chelating agents</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">EDTA</li>
                                    <li class="item-subProducts font-titles-md">sodium gluconate</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">starches</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">corn starch</li>
                                    <li class="item-subProducts font-titles-md">potato starch</li>
                                    <li class="item-subProducts font-titles-md">wheat starch</li>
                                    <li class="item-subProducts font-titles-md">tapioca starch</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">fruits in pieces / powder</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aguaymanto</li>
                                    <li class="item-subProducts font-titles-md">blueberry</li>
                                    <li class="item-subProducts font-titles-md">Camu Camu</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">lifting agents</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">ammonium bicarbonate</li>
                                    <li class="item-subProducts font-titles-md">sodium bicarbonate</li>
                                    <li class="item-subProducts font-titles-md">gluconodeltalactone</li>
                                    <li class="item-subProducts font-titles-md">sodium acid pyrophoshate</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">modified starches</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">potato maltodextrine</li>
                                    <li class="item-subProducts font-titles-md">corn maltodextrine</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">maskers</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">sweeteners</li>
                                    <li class="item-subProducts font-titles-md">Soy</li>
                                    <li class="item-subProducts font-titles-md">vitamins and minerals</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">spices</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">for sausages</li>
                                    <li class="item-subProducts font-titles-md">for marinates</li>
                                    <li class="item-subProducts font-titles-md">for curing salts</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">sweeteners</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">acesulfame</li>
                                    <li class="item-subProducts font-titles-md">aspartame</li>
                                    <li class="item-subProducts font-titles-md">sodium cyclamate</li>
                                    <li class="item-subProducts font-titles-md">anhydrous dextrose</li>
                                    <li class="item-subProducts font-titles-md">monohydrate dextrose</li>
                                    <li class="item-subProducts font-titles-md">glucose</li>
                                    <li class="item-subProducts font-titles-md">sodium saccharine</li>
                                    <li class="item-subProducts font-titles-md">sorbitol</li>
                                    <li class="item-subProducts font-titles-md">Stevia</li>
                                    <li class="item-subProducts font-titles-md">sucralose</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">stabilizers</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">sodium alginate</li>
                                    <li class="item-subProducts font-titles-md">sodium chloride</li>
                                    <li class="item-subProducts font-titles-md">CMC</li>
                                    <li class="item-subProducts font-titles-md">cream of tartar</li>
                                    <li class="item-subProducts font-titles-md">Gelatin</li>
                                    <li class="item-subProducts font-titles-md">Gelatin 230 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatin 260 BLOOM</li>
                                    <li class="item-subProducts font-titles-md">Gelatin 280 BLOOM </li>
                                    <li class="item-subProducts font-titles-md">arabic gum</li>
                                    <li class="item-subProducts font-titles-md">guar gum</li>
                                    <li class="item-subProducts font-titles-md">xanthan gum</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">antioxidants and preservatives</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">ascorbic acid</li>
                                    <li class="item-subProducts font-titles-md">sorbic acid</li>
                                    <li class="item-subProducts font-titles-md">sodium ascorbate</li>
                                    <li class="item-subProducts font-titles-md">sodium benzoate</li>
                                    <li class="item-subProducts font-titles-md">potassium benzoate</li>
                                    <li class="item-subProducts font-titles-md">BHT</li>
                                    <li class="item-subProducts font-titles-md">sodium citrate</li>
                                    <li class="item-subProducts font-titles-md">diacetates</li>
                                    <li class="item-subProducts font-titles-md">sodium erythorbate</li>
                                    <li class="item-subProducts font-titles-md">potassium lactate</li>
                                    <li class="item-subProducts font-titles-md">Sodium Lactate</li>
                                    <li class="item-subProducts font-titles-md">sodium metabisulphite</li>
                                    <li class="item-subProducts font-titles-md">natamycin</li>
                                    <li class="item-subProducts font-titles-md">sodium nitrite</li>
                                    <li class="item-subProducts font-titles-md">calcium propionate</li>
                                    <li class="item-subProducts font-titles-md">potassium sorbate</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">flavors and enhancers</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Anethol</li>
                                    <li class="item-subProducts font-titles-md">coffee</li>
                                    <li class="item-subProducts font-titles-md">anhydrous caffeine</li>
                                    <li class="item-subProducts font-titles-md">Carnes Diversas</li>
                                    <li class="item-subProducts font-titles-md">Citrus</li>
                                    <li class="item-subProducts font-titles-md">spices</li>
                                    <li class="item-subProducts font-titles-md">eucaliptol</li>
                                    <li class="item-subProducts font-titles-md">blue fruits</li>
                                    <li class="item-subProducts font-titles-md">peruvian / tropical fruits</li>
                                    <li class="item-subProducts font-titles-md">red / green / orange fruits</li>
                                    <li class="item-subProducts font-titles-md">Humo</li>
                                    <li class="item-subProducts font-titles-md">menthol</li>
                                    <li class="item-subProducts font-titles-md">cheese</li>
                                    <li class="item-subProducts font-titles-md">green/black tee</li>
                                    <li class="item-subProducts font-titles-md">tutti frutti</li>
                                    <li class="item-subProducts font-titles-md">vainillina / ethyl vainillina
                                    </li>
                                    <li class="item-subProducts font-titles-md">vegetables</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">emulsifiers</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">soy lecithin</li>
                                    <li class="item-subProducts font-titles-md">mono and diglycerids</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">dairy products</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">skimmed milk</li>
                                    <li class="item-subProducts font-titles-md">full cream milk</li>
                                    <li class="item-subProducts font-titles-md">deproteinized whey</li>
                                    <li class="item-subProducts font-titles-md">whey</li>
                                    <li class="item-subProducts font-titles-md">milk replacer</li>
                                    <li class="item-subProducts font-titles-md">milk replacer</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">moisturizers</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">glycerine</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">anti compacting agents</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">tricalcium phosphate</li>
                                    <li class="item-subProducts font-titles-md">silica satls</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">surfactants</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">polisorbate</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">lubricants</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">vaseline</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">non cholesterol</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">down col</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="../assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="../assets/images/icons/forma_esquina.svg" alt="">
        </section>
    </main>
    <?php
        include 'includes/footer.php'
    ?>
    <script src="../assets/js/accordion.js"></script>
</body>

</html>