<?php
    $data =  array(
        'pagetitle' => 'Productos - Cuidado Personal',
        'meta_description' => 'Amplia cartera de productos para la industria cosmética',
        'meta_keywords' => 'Tripolifosfato, industria de cosmética, industria de cuidado personal',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="../assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="../assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">high</h1>
                <span class="subtitle-banner font-titles-md textUppercase">quality supplies</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-cuidado-personal"></i>
                                <h1 class="title-primary titles-big">PERSONAL</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">care</h2>
                            </div>
                            <div class="img-products content-img-two d-none d-lg-block wow zoomIn">
                                <img src="../assets/images/internas/bajas/cuidado-personal1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">products</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Citric acid</li>
                                    <li class="item-subProducts font-titles-md">Functional acids</li>
                                    <li class="item-subProducts font-titles-md">Triethyl Citrate</li>
                                    <li class="item-subProducts font-titles-md">Tributyl Citrate</li>
                                    <li class="item-subProducts font-titles-md">Monosodium citrate</li>
                                    <li class="item-subProducts font-titles-md">Tricalcium citrate</li>
                                    <li class="item-subProducts font-titles-md">Trimagnesium citrate</li>
                                    <li class="item-subProducts font-titles-md">Trisodium citrate</li>
                                    <li class="item-subProducts font-titles-md">Collagen</li>
                                    <li class="item-subProducts font-titles-md">Potassium gluconate</li>
                                    <li class="item-subProducts font-titles-md">Sodium gluconate</li>
                                    <li class="item-subProducts font-titles-md">Gluconodeltalactone</li>
                                    <li class="item-subProducts font-titles-md">Potassium lactate</li>
                                    <li class="item-subProducts font-titles-md">Sodium lactate</li>
                                    <li class="item-subProducts font-titles-md">Sodium lauril sulphate</li>
                                    <li class="item-subProducts font-titles-md">Polysorbates</li>
                                    <li class="item-subProducts font-titles-md">Functional salts</li>
                                    <li class="item-subProducts font-titles-md">Citrate zinc</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">products</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Citric acid</li>
                                    <li class="item-subProducts font-titles-md">Functional acids</li>
                                    <li class="item-subProducts font-titles-md">Triethyl Citrate</li>
                                    <li class="item-subProducts font-titles-md">Tributyl Citrate</li>
                                    <li class="item-subProducts font-titles-md">Monosodium citrate</li>
                                    <li class="item-subProducts font-titles-md">Tricalcium citrate</li>
                                    <li class="item-subProducts font-titles-md">Trimagnesium citrate</li>
                                    <li class="item-subProducts font-titles-md">Trisodium citrate</li>
                                    <li class="item-subProducts font-titles-md">Collagen</li>
                                    <li class="item-subProducts font-titles-md">Potassium gluconate</li>
                                    <li class="item-subProducts font-titles-md">Sodium gluconate</li>
                                    <li class="item-subProducts font-titles-md">Gluconodeltalactone</li>
                                    <li class="item-subProducts font-titles-md">Potassium lactate</li>
                                    <li class="item-subProducts font-titles-md">Sodium lactate</li>
                                    <li class="item-subProducts font-titles-md">Sodium lauril sulphate</li>
                                    <li class="item-subProducts font-titles-md">Polysorbates</li>
                                    <li class="item-subProducts font-titles-md">Functional salts</li>
                                    <li class="item-subProducts font-titles-md">Citrate zinc</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="../assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="../assets/images/icons/forma_esquina.svg" alt="">

        </section>
    </main>
    <?php
        include 'includes/footer.php'
    ?>
    <script src="../assets/js/accordion.js"></script>
</body>

</html>