<?php
    $data =  array(
        'pagetitle' => 'Servicios - Información Actual del Mercado',
        'meta_description' => 'Precios actualizados de acuerdo al mercado',
        'meta_keywords' => '',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/informacion_de_mercado.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">INFORMACIÓN</h1>
                <span class="subtitle-banner font-titles-md textUppercase">DE MERCADO</span>
            </div>
        </section>
        <?php
            include 'src/includes/navbar-services-int.php'
        ?>
        <section class="sct-info-services overflow-h">
            <div class="container px-0">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-12 col-lg-11">
                                <div class="row justify-content-center">
                                    <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                        <i class="icon-descr icon-info-de-mercad"></i>
                                        <h1 class="titles-big color-secondary">Actualización</h1>
                                        <span class="span-desc font-internas textUppercase">DE INFORMACIÓN</span>
                                        <span class="subtitle-internas font-titles-md textUppercase">GLOBALIZADA</span>
                                    </div>
                                    <div class="col-12 col-lg-11 wow fadeInLeft">
                                        <p class="font-internas">Proveemos información de mercado actualizada, facilitando a nuestros clientes la toma de decisiones de compra, a fin de obtener precios competitivos y evitar desabastecimientos.
                                            Nuestra amplia red de proveedores a nivel mundial nos permite atender cualquier producto solicitado por nuestros clientes. desde el envío de la información técnica del producto (especificaciones técnicas, certificaciones y demás) y homologación de muestras, hasta la visita presencial 
                                            de nuestros técnicos a las fábricas y/o proveedores para garantizar la calidad del producto.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-11 col-md-9 col-lg-6 px-0 wrapper-img-servDscrp">
                        <div class="content-img-servDscrp wow zoomIn">
                            <img src="assets/images/internas/bajas/informacion_desc.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>