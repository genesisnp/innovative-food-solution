<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Innovate Food Solutions</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/> 

    </head>
 
    <body style="margin: 0; padding: 0;">
        <table  align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="border-collapse: collapse;" bgcolor="#fff">
            <tr><td bgcolor="#fff" style="height: 3em;"></td></tr>
            <tr>
                <td  style="text-align: center">
                    <table style="width:100%; margin-top:1em;margin-bottom: 2em;" cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
                        <tr>
                            <td width="200">&nbsp;&nbsp;</td>
                            <td><img src="<?= base_url() ?>assets/images/logo-innovative.png" style="width: 100%"></td>
                            <td width="200">&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                </td>     
            </tr>  
            <tr><td bgcolor="#fff" style="height: 3em;"></td></tr>            
            <tr>   
                <td style="text-align: center;font-family: Arial;">
                    <p style="font-size: 30px;; font-weight: 600;margin-bottom: 0;"><?= $nombres ?></p>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width:100%; margin-top:1em;margin-bottom: 2em;" cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td style="color: #44850f; font-weight:600;text-align:right;font-family: Arial;">Empresa&nbsp; : </td>
                            <td style="text-align:left;font-family: Arial;">&nbsp; <?= $empresa ?></td>
                            <td width="60">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td style="color: #44850f; font-weight:600;text-align:right;font-family: Arial;">E-mail&nbsp; : </td>
                            <td style="text-align:left;font-family: Arial;">&nbsp; <?= $email ?></td>
                            <td width="60">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td style="color: #44850f; font-weight:600;text-align:right;font-family: Arial;">Teléfono&nbsp; : </td>
                            <td style="text-align:left;font-family: Arial;">&nbsp; <?= $telefono ?></td>
                            <td width="60">&nbsp;&nbsp;</td>
                        </tr>
						<tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td style="color: #44850f; font-weight:600;text-align:right;font-family: Arial;">Adjunto&nbsp; : </td>
                            <td style="text-align:left;font-family: Arial;">&nbsp; <?= $adjunto ?></td>
                            <td width="60">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td style="color: #44850f; font-weight:600;text-align:right;font-family: Arial;">Mensaje&nbsp; : </td>
                            <td style="text-align:left;font-family: Arial;">&nbsp; <?= $mensaje?></td>
                            <td width="60">&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><img src="<?= base_url() ?>assets/images/mailing/b1.jpg" style="width: 100%"></td>
            </tr>
        </table>       
    </body>
</html>
