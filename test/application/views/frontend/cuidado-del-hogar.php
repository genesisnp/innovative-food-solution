<?php
    $data =  array(
        'pagetitle' => 'Productos - Cuidado del Hogar y Otros',
        'meta_description' => 'Amplia cartera de productos para la industria del cuidado del hogar',
        'meta_keywords' => 'Tripolifosfato',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">insumos</h1>
                <span class="subtitle-banner font-titles-md textUppercase">de alta calidad</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'src/includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-cuidado-hogar"></i>
                                <h1 class="title-primary titles-big">CUIDADO</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">del hogar y otros</h2>
                            </div>
                            <div class="img-products content-img-two d-none d-lg-block wow zoomIn">
                                <img src="assets/images/internas/bajas/limpieza.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">productos</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Bórico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Glucónico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Láctico</li>
                                    <li class="item-subProducts font-titles-md">Citrato Trisódico</li>
                                    <li class="item-subProducts font-titles-md">Cloruro de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Cumarina</li>
                                    <li class="item-subProducts font-titles-md">Fosfato Diamónico</li>
                                    <li class="item-subProducts font-titles-md">Fosfato Monoamónico</li>
                                    <li class="item-subProducts font-titles-md">Fragancias</li>
                                    <li class="item-subProducts font-titles-md">Gluconato de sodio</li>
                                    <li class="item-subProducts font-titles-md">Hidróxido de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Hipoclorito de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Hipoclorito de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Nitrato de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Nitrato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Polisorbatos</li>
                                    <li class="item-subProducts font-titles-md">Sales y Ácidos funcionales</li>
                                    <li class="item-subProducts font-titles-md">Silicas</li>
                                    <li class="item-subProducts font-titles-md">Sulfato de Mg/Al/Mn/Fe</li>
                                    <li class="item-subProducts font-titles-md">Talco en polvo</li>
                                    <li class="item-subProducts font-titles-md">Tierra ﬁltrante</li>
                                    <li class="item-subProducts font-titles-md">Tripolifosfato de Sodio</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">productos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Bórico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Glucónico</li>
                                    <li class="item-subProducts font-titles-md">Ácido Láctico</li>
                                    <li class="item-subProducts font-titles-md">Citrato Trisódico</li>
                                    <li class="item-subProducts font-titles-md">Cloruro de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Cumarina</li>
                                    <li class="item-subProducts font-titles-md">Fosfato Diamónico</li>
                                    <li class="item-subProducts font-titles-md">Fosfato Monoamónico</li>
                                    <li class="item-subProducts font-titles-md">Fragancias</li>
                                    <li class="item-subProducts font-titles-md">Gluconato de sodio</li>
                                    <li class="item-subProducts font-titles-md">Hidróxido de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Hipoclorito de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Hipoclorito de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Nitrato de Calcio</li>
                                    <li class="item-subProducts font-titles-md">Nitrato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Polisorbatos</li>
                                    <li class="item-subProducts font-titles-md">Sales y Ácidos funcionales</li>
                                    <li class="item-subProducts font-titles-md">Silicas</li>
                                    <li class="item-subProducts font-titles-md">Sulfato de Mg/Al/Mn/Fe</li>
                                    <li class="item-subProducts font-titles-md">Talco en polvo</li>
                                    <li class="item-subProducts font-titles-md">Tierra ﬁltrante</li>
                                    <li class="item-subProducts font-titles-md">Tripolifosfato de Sodio</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="assets/images/icons/forma_esquina.svg" alt="">
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>