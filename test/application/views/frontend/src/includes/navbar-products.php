<section class="sct-navbar-products">
    <div class="wow zoomIn wrapper-navP <?= in_array('industria-alimentaria', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="assets/images/spiner.png" alt="">
        <a href="industria-alimentaria" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-ind-alimentaria"></i>
                <h1 class="font-titles-md textUppercase">industria <br>alimentaria</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('nutricion-y-salud', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="assets/images/spiner.png" alt="">
        <a href="nutricion-y-salud" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-nutricion"></i>
                <h1 class="font-titles-md textUppercase">nutricion<br>y salud</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('cuidado-personal', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="assets/images/spiner.png" alt="">
        <a href="cuidado-personal" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cuidado-personal"></i>
                <h1 class="font-titles-md textUppercase">cuidado<br>personal</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('cuidado-del-hogar', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="assets/images/spiner.png" alt="">
        <a href="cuidado-del-hogar" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cuidado-hogar"></i>
                <h1 class="font-titles-md textUppercase">cuidado<br>del hogar<br>y otros</h1>
            </div>
        </a>
    </div>
</section>