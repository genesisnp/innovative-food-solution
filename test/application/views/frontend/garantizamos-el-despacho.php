<?php
    $data =  array(
        'pagetitle' => 'Servicios - Garantizamos el Despacho',
        'meta_description' => 'Especializados en brindar soluciones logísiticas en la importacion de ingredientes, insumos y aditivos para la industria',
        'meta_keywords' => '',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/despacho.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">garantizamos</h1>
                <span class="subtitle-banner font-titles-md textUppercase">EL DESPACHO</span>
            </div>
        </section>
        <?php
            include 'src/includes/navbar-services-int.php'
        ?>
        <section class="sct-info-services overflow-h">
            <div class="container px-0">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-12 col-lg-11">
                                <div class="row justify-content-center">
                                    <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                        <i class="icon-descr icon-despacho"></i>
                                        <h1 class="titles-big color-secondary">Especialistas</h1>
                                        <span class="span-desc font-internas textUppercase">EN ESTRUCTURAR</span>
                                        <span class="subtitle-internas font-titles-md textUppercase">SOLUCIONES
                                            LOGÍSTICAS</span>
                                    </div>
                                    <div class="col-12 col-lg-11 wow fadeInLeft">
                                        <p class="font-internas">Ya sea vía marítima, aérea, terrestre o multimodal que
                                            mejor se adapte a las necesidades de nuestros clientes, con el fin de
                                            garantizar de manera óptima el despacho del producto en tiempo, costo y
                                            forma.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-11 col-md-9 col-lg-6 px-0 wrapper-img-servDscrp">
                        <div class="content-img-servDscrp wow zoomIn">
                            <img src="assets/images/internas/bajas/despacho_desc.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>