<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Suscriptores extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('backend/sistema');
        $this->load->model('backend/msuscriptores');
        $this->load->helper('general');

        if ($this->session->has_userdata('manager')) {
            $this->manager = $this->session->userdata('manager');
        } else {
            redirect('manager');
        }
    }

    public function index() {
        $user=$this->manager['user']['idperfil'];
//        print_r($user); exit;
        $idmodulo=11;
        
        $data = array();
        $data['permiso']=$this->sistema->getPermisos($user,$idmodulo);
        $data['modulos']=$this->sistema->getModulos($user);
        
        $output = $this->load->view('backend/suscriptores', $data, TRUE);

        return $this->__output($output);
    }


    public function read() {
        $draw = $this->input->post('draw', TRUE);
        $search = $this->input->post('search', TRUE);
        $start = (int) $this->input->post('start', TRUE);
        $length = (int) $this->input->post('length', TRUE);
        
        $user=$this->manager['user']['idperfil'];
        $idmodulo=11;
        
        $permiso=$this->sistema->getPermisos($user,$idmodulo);
        
        $suscriptores = $this->msuscriptores->getSuscriptores($search['value'], $length, $start);
        
        $data = array();

        foreach ($suscriptores as $suscriptor) {
            $suscriptor['fechajm']=(new DateTime($suscriptor['fecha']))->format('d/m/Y H:i:s');

            $data[] = $suscriptor;
        }

        $dataObj = array(
            'draw' => $draw,
            'recordsTotal' => $this->msuscriptores->getTotal(),
            'recordsFiltered' => $this->msuscriptores->getTotal($search['value']),
            'data' => $data
        );

        $this->output->set_content_type('application/json');

        return $this->__output(json_encode($dataObj));
    }
    
    public function exportarsus()
    {
        setlocale(LC_ALL, 'es_PE');
        $nom=$this->input->get('nom');
        $data = $this->msuscriptores->getSuscriptoresExcel($nom);
        $registros = array();

        foreach ($data as $contacto) {

            $registros[] = $contacto;
        }

        $salida = '<table border="1">';
        $salida .= '<tr>';
        $salida .= '<td>NOMBRES</td>';
        $salida .= '<td>E-MAIL</td>';
        $salida .= '<td>FECHA</td>';
        $salida .= '</tr>';

        foreach ($registros as $i => $registro) {
            $salida .= '<tr>';
            $salida .= '<td>' . utf8_decode($registro['nombres']) . '</td>';
            $salida .= '<td>' . utf8_decode($registro['email']) . '</td>';
            $salida .= '<td>' . $registro['fecha'] . '</td>';
            $salida .= '</tr>';
        }

        $salida .= '</table>';

        $this->output->set_header("Content-Disposition: attachment; filename=reporte_" . date('Y-m-d') . ".xls");
        $this->output->set_content_type('application/vnd.ms-excel');
        $this->output->set_output($salida);
    }
  
    private function __output($html = NULL) {
        if (ENVIRONMENT === 'production') {
            $html = minifyHtml($html);
        }

        $this->output->set_output($html);
    }

}
