<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Enviar extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('My_PHPMailer');

//        $this->load->model('frontend/contenido');
//        $this->load->model('frontend/taxonomia');
//        $this->load->model('frontend/mcontactos');
        $this->load->model('frontend/menviar');

//        if ($this->session->has_userdata('contenido')) {
//            $this->contenidox = $this->session->userdata('contenido');
//        } else {
//            $contenido = $this->contenido->getContenido('all');
//            $this->contenidox = $contenido;
//
//            $this->session->set_userdata('contenido', $this->contenidox);
//        }
//
//        if ($this->session->has_userdata('usuario')) {
//            $this->usuario = $this->session->userdata('usuario');
//        } else {
//            $this->usuario = array();
//        }
//
//        if ($this->session->has_userdata('carrito')) {
//            $this->carrito = $this->session->userdata('carrito');
//        } else {
//            $this->carrito = array();
//        }
    }

    public function index() {
//        $data = array();
//
//        $output = $this->load->view('frontend/inicio', $data, TRUE);
//
//        return $this->__output($output);
    }
	
	
	public function contacto(){
        $post = $this->input->post(NULL, TRUE);
        //print_r($_FILES); exit;
		
        if (!empty($post)) {
			
			$arrLoad = $this->upLoadFile();
			$fileLoad = "";			
			if ($arrLoad['status'] == 1) {			
				$fileLoad = $arrLoad['path'];	
			} else {
				//$fileLoad =  $arrLoad['status']." - ".$arrLoad['error_msg'];
				$fileLoad =  '';
			}
			
			$post['adjunto'] = $fileLoad;
			
            $dataMail = array(
                'remitente' => array(
                    'email' => 'no-reply@innovativefoodsolutions.es',
                    'nombre' => 'Web master'
                ),
                'destinatarios' => array(
                    array(
                    'email' => 'soporte@exe.pe',
                    'nombre' => 'soporte test'
                    )
                ),
                'asunto' => 'Nuevo mensaje de Contactenos',
                'mensaje' => $this->load->view('frontend/mail-contactenos', $post, TRUE),
				'full_path' => isset ($arrLoad['full_path']) ? $arrLoad['full_path'] : '',
				'path' => isset ($arrLoad['path']) ? $arrLoad['path'] : ''
            );
            
            $fecha = (new DateTime())->format('Y-m-d H:i:s');
			
			unset($post['acepta']);
            $post['fecha'] = $fecha;			 
			$post['tipo'] = 'contactenos';
					
            $guardar = $this->menviar->saveFormulario($post);
           
            if($guardar){
                $this->sendMail($dataMail);    
            }
            //return $this->redireccionar($post['page']);
        } else {
            //return $this->redireccionar();
        }
    }
    


    public function suscribirme(){
        $post = $this->input->post(NULL, TRUE);
//       print_r($post); exit;
        if (!empty($post)) {
            $dataMail = array(
                'remitente' => array(
                    'email' => 'no-reply@neoagrum.com.pe',
                    'nombre' => 'Web master'
                ),
                'destinatarios' => array(
                    array(
                    'email' => 'marketing@neoagrum.com.pe',
                    'nombre' => 'Marketing'
                    )
                ),
                'asunto' => 'Nuevo mensaje de Suscribete',
                'mensaje' => $this->load->view('frontend/chunks/mail-suscribete', $post, TRUE)
            );
            
            $fecha = (new DateTime())->format('Y-m-d H:i:s');
            
            $post['fecha'] = $fecha;
            $post['tipo'] = 'suscribete';
            $guardar = $this->mregistro->saveFormulario($post);
           
            if($guardar){
                $this->sendMail($dataMail);    
            }
            //return $this->redireccionar($post['page']);
        } else {
            //return $this->redireccionar();
        }
    }
	
	public function upLoadFile(){
		
			$data = array();			
			//load form validation library
			$this->load->library('form_validation');
			
			//load file helper
			$this->load->helper('file');
        
			$config['upload_path']   = 'assets/sources/archivosAdj';
			$config['allowed_types'] = 'gif|jpg|png|pdf|docx|jpeg';
			$config['max_size']      = 20480;//1024;
			$this->load->library('upload', $config);
			
			//upload file to directory
			if($this->upload->do_upload('adjuntar')){
				$uploadData = $this->upload->data();
				$uploadedFile = $uploadData['file_name'];
				
				$data['status'] = 1;
				$data['path'] = $uploadData['file_name'];
				$data['full_path'] = $uploadData['full_path'];
			}else{
				$data['status'] = 0;
				$data['error_msg'] = $this->upload->display_errors();
			}
			
        	return $data;
	}

//    public function amigo() {
//        $post = $this->input->post(NULL, TRUE);
//
//        if (isset($post) && !empty($post)) {
//            $datax = array(
//                'remitente' => array(
//                    'email' => $post['enviar_correo'],
//                    'nombre' => $post['enviar_nombre']
//                ),
//                'destinatarios' => array(
//                    array(
//                        'email' => $post['enviar_correoamigo'],
//                        'nombre' => $post['enviar_nombreamigo']
//                    ),
//                ),
//                'asunto' => 'Tu amigo ' . $post['enviar_nombre'] . ', te recomienda este producto',
//                'mensaje' => $this->load->view('frontend/chunks/mail-recomendar-amigo', $post, TRUE)
//            );
//
//            $this->sendMail($datax);
//        }
//
////        $output = $this->load->view('frontend/inicio', $data, TRUE);
////
////        return $this->__output($output);
//    }

    private function sendMail($data = array()) {
        $mailer = new PHPMailer();

        if (!empty($data)) {
            try {
                $mailer->CharSet = 'UTF-8';
                $mailer->isHTML(true);
                $mailer->setFrom($data['remitente']['email'], $data['remitente']['nombre']);

                foreach ($data['destinatarios'] as $key => $destinatario) {
                    $mailer->addAddress($destinatario['email'], $destinatario['nombre']);
                }

            //$mailer->AddBCC("soporte@exe.pe", "Ventas Florerias");
				if(isset( $data['full_path'])){
					$mailer->AddAttachment( $data['full_path'], $data['path'] );
				}
                $mailer->Subject = $data['asunto'];
                $mailer->msgHTML($data['mensaje']);

                return $mailer->send();
            } catch (Exception $e) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mailer->ErrorInfo;
            }
        }
    }
    
    private function guardarRegistro($post = array())
    {
        $fecha=(new DateTime())->format('Y-m-d H:i:s');
        $res=json_encode($post);
        $data=array(
          'formulario'=>'Contactos',
          'data'=>$res,
          'fecha'=>$fecha  
        );
        return $this->mcontactos->guardarEnvio($data);
    }

    private function __output($html = NULL) {
        if (ENVIRONMENT === 'production') {
            $html = minifyHtml($html);
        }

        $this->output->set_output($html);
    }

}
