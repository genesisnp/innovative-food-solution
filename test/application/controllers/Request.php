<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

	public function __construct() {
            parent::__construct();

            $this->load->model('frontend/taxonomia');
            $this->load->model('frontend/contenido');
            $this->load->model('backend/mvisitas');
            $this->load->helper('general');
			
//          $this->load->model('frontend/productos');
//          $this->load->model('frontend/usuarios');
            
            /*if ($this->session->has_userdata('contenido')) {
                $this->contenidox = $this->session->userdata('contenido');
            } else {
                $contenido = $this->contenido->getContenido('all');
                $this->contenidox = $contenido;

                $this->session->set_userdata('contenido', $this->contenidox);
            }*/

			

        }
        
	public function index($uri = false){
			
            /*$pagina=$this->taxonomia->getPaginaurl($this->uri->uri_string);
			
            
            if(empty($pagina) && !isset($pagina)){
                return $this->pageError();
            }            
            */
			
            /*$data = array(
                'contenido'=>$this->contenido->getContenido($pagina['idpagina']),
                'pagina'=>$pagina,
                'conte'=>$this->contenido->getContenido(1),
                'contejm'=>$this->contenido->getContenido(15)
            );

            $categoria=$this->contenido->getCategoria($pagina['idsitemap']);
            
            if(isset($categoria) && !empty($categoria)){
                $data['categoria']=$categoria;
            }
            
            $data['libros']=$this->contenido->getLibros();
            $data['articulos']=$this->contenido->getArticulos($pagina['idpagina'],16,0);
            $data['cantidad'] = count($this->contenido->getArticulos($pagina['idpagina']));

            $pag=$pagina['template'];*/
			
            //print_r($data['articulos']);exit;
			
			$data = array();
			if(!$uri){
				$uri = $this->uri->uri_string;
			}else{
				if($uri == 'en'){
					$uri = 'en/index';
				}
			}
			
			
			if(empty($uri)){
				$uri = 'en/index';
			}
			
			//echo $uri;
			
            $output = $this->load->view('frontend/'.$uri.'', $data, TRUE);

            $this->output->set_header('Access-Control-Allow-Origin: *');
            return $this->__output($output);
	}
        

        
        public function pageError() {
            $data = array(
                'conte'=>$this->contenido->getContenido(1)
            );
            $this->load->view('frontend/404',$data);
        }
        
        
        private function __output($html = NULL) {
        if (ENVIRONMENT === 'production') {
            $html = minifyHtml($html);
        }

        $this->output->set_output($html);
    }
}
