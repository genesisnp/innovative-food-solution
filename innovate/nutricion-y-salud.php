<?php
    $data =  array(
        'pagetitle' => 'Productos - Nutrición y Salud',
        'meta_description' => 'Amplia cartera de produtos para la industria de Nutrición Animal',
        'meta_keywords' => 'Goma Xantan, Ácido Láctico, Citratos',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">insumos</h1>
                <span class="subtitle-banner font-titles-md textUppercase">de alta calidad</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'src/includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-ind-alimentaria"></i>
                                <h1 class="title-primary titles-big">NUTRICIÓN</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">Y SALUD</h2>
                            </div>
                            <div class="img-products content-img-one d-none d-lg-block wow zoomIn">
                                <img src="assets/images/internas/bajas/nutricion-1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">Ácidos Grasos Esenciales</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Alfa Lipóico</li>
                                    <li class="item-subProducts font-titles-md">ARA</li>
                                    <li class="item-subProducts font-titles-md">DHA</li>
                                    <li class="item-subProducts font-titles-md">GABA</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Derivados de Aminoácidos</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">L-Carnitina</li>
                                    <li class="item-subProducts font-titles-md">L-Creatina</li>
                                    <li class="item-subProducts font-titles-md">L-Ornitina</li>
                                    <li class="item-subProducts font-titles-md">L-Teanina</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Extractos Vegetales Funcionales</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aloe Vera</li>
                                    <li class="item-subProducts font-titles-md">Chitosán</li>
                                    <li class="item-subProducts font-titles-md">Chondroitin Sulfato</li>
                                    <li class="item-subProducts font-titles-md">Equinácea</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Dong Quai</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Ganoderma Lucidum</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Ginkgo Biloba</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Té verde/Té negro</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Tribulis Terrestris</li>
                                    <li class="item-subProducts font-titles-md">Garcinia Cambogia</li>
                                    <li class="item-subProducts font-titles-md">Glucosamina Sulfato</li>
                                    <li class="item-subProducts font-titles-md">Isoflavones de soya</li>
                                    <li class="item-subProducts font-titles-md">Micelio de Cordyceps</li>
                                    <li class="item-subProducts font-titles-md">Resveratrol 98%</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Farma</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico Citrato de </li>
                                    <li class="item-subProducts font-titles-md">Sodio Gluconato de </li>
                                    <li class="item-subProducts font-titles-md">Potasio Glucona-Delta-</li>
                                    <li class="item-subProducts font-titles-md">Lactona Citrato de Zinc</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Proteínas y Aminoácidos</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparagina</li>
                                    <li class="item-subProducts font-titles-md">Colágeno</li>
                                    <li class="item-subProducts font-titles-md">L-Arginina</li>
                                    <li class="item-subProducts font-titles-md">L-Cisteína</li>
                                    <li class="item-subProducts font-titles-md">L-Glicina</li>
                                    <li class="item-subProducts font-titles-md">L-Glutamina</li>
                                    <li class="item-subProducts font-titles-md">L-Isoleucina</li>
                                    <li class="item-subProducts font-titles-md">L-Leucina</li>
                                    <li class="item-subProducts font-titles-md">L-Lisina</li>
                                    <li class="item-subProducts font-titles-md">L-Taurina</li>
                                    <li class="item-subProducts font-titles-md">L-Triftófano</li>
                                    <li class="item-subProducts font-titles-md">L-Valina</li>
                                    <li class="item-subProducts font-titles-md">N-Acetil Cisteína (NAC)</li>
                                    <li class="item-subProducts font-titles-md">Proteína aislada de soya</li>
                                    <li class="item-subProducts font-titles-md">funcional y no funcional 90%</li>
                                    <li class="item-subProducts font-titles-md">Actino (CPM Nitro)</li>
                                    <li class="item-subProducts font-titles-md">Proteína de suero lácteo</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Vitaminas y Minerales</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Bioferrin</li>
                                    <li class="item-subProducts font-titles-md">Citrato Tricalcico</li>
                                    <li class="item-subProducts font-titles-md">Cloruro de Colina 98%</li>
                                    <li class="item-subProducts font-titles-md">Complejo de Calcio lácteo</li>
                                    <li class="item-subProducts font-titles-md">Inositol</li>
                                    <li class="item-subProducts font-titles-md">Bioferrin</li>
                                    <li class="item-subProducts font-titles-md">Óxido de Zinc</li>
                                    <li class="item-subProducts font-titles-md">Vitaminas A, B, C, D3</li>
                                    <li class="item-subProducts font-titles-md">Mezclas de Vitaminas - Tailor made</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Ácidos Grasos<br>Esenciales</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Alfa Lipóico</li>
                                    <li class="item-subProducts font-titles-md">ARA</li>
                                    <li class="item-subProducts font-titles-md">DHA</li>
                                    <li class="item-subProducts font-titles-md">GABA</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Derivados de Aminoácidos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">L-Carnitina</li>
                                    <li class="item-subProducts font-titles-md">L-Creatina</li>
                                    <li class="item-subProducts font-titles-md">L-Ornitina</li>
                                    <li class="item-subProducts font-titles-md">L-Teanina</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Extractos Vegetales<br>Funcionales</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aloe Vera Chitosán</li>
                                    <li class="item-subProducts font-titles-md">Chondroitin Sulfato</li>
                                    <li class="item-subProducts font-titles-md">Equinácea</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Dong Quai</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Ganoderma Lucidum</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Ginkgo Biloba</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Té verde/Té negro</li>
                                    <li class="item-subProducts font-titles-md">Extracto de Tribulis Terrestris</li>
                                    <li class="item-subProducts font-titles-md">Garcinia Cambogia</li>
                                    <li class="item-subProducts font-titles-md">Glucosamina Sulfato</li>
                                    <li class="item-subProducts font-titles-md">Isoflavones de soya</li>
                                    <li class="item-subProducts font-titles-md">Micelio de Cordyceps</li>
                                    <li class="item-subProducts font-titles-md">Resveratrol 98%</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Farma</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico Citrato de </li>
                                    <li class="item-subProducts font-titles-md">Sodio Gluconato de </li>
                                    <li class="item-subProducts font-titles-md">Potasio Glucona-Delta-</li>
                                    <li class="item-subProducts font-titles-md">Lactona Citrato de Zinc</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Proteínas y<br>Aminoácidos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparagina</li>
                                    <li class="item-subProducts font-titles-md">Colágeno</li>
                                    <li class="item-subProducts font-titles-md">L-Arginina</li>
                                    <li class="item-subProducts font-titles-md">L-Cisteína</li>
                                    <li class="item-subProducts font-titles-md">L-Glicina</li>
                                    <li class="item-subProducts font-titles-md">L-Glutamina</li>
                                    <li class="item-subProducts font-titles-md">L-Isoleucina</li>
                                    <li class="item-subProducts font-titles-md">L-Leucina</li>
                                    <li class="item-subProducts font-titles-md">L-Lisina</li>
                                    <li class="item-subProducts font-titles-md">L-Taurina</li>
                                    <li class="item-subProducts font-titles-md">L-Triftófano</li>
                                    <li class="item-subProducts font-titles-md">L-Valina</li>
                                    <li class="item-subProducts font-titles-md">N-Acetil Cisteína (NAC)</li>
                                    <li class="item-subProducts font-titles-md">Proteína aislada de soya</li>
                                    <li class="item-subProducts font-titles-md">funcional y no funcional 90%</li>
                                    <li class="item-subProducts font-titles-md">Actino (CPM Nitro)</li>
                                    <li class="item-subProducts font-titles-md">Proteína de suero lácteo</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Vitaminas y Minerales</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Bioferrin</li>
                                    <li class="item-subProducts font-titles-md">Citrato Tricalcico</li>
                                    <li class="item-subProducts font-titles-md">Cloruro de Colina 98%</li>
                                    <li class="item-subProducts font-titles-md">Complejo de Calcio lácteo</li>
                                    <li class="item-subProducts font-titles-md">Inositol</li>
                                    <li class="item-subProducts font-titles-md">Bioferrin</li>
                                    <li class="item-subProducts font-titles-md">Óxido de Zinc</li>
                                    <li class="item-subProducts font-titles-md">Vitaminas A, B, C, D3</li>
                                    <li class="item-subProducts font-titles-md">Mezclas de Vitaminas - Tailor made</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="assets/images/icons/forma_esquina.svg" alt="">
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>
