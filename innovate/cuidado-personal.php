<?php
    $data =  array(
        'pagetitle' => 'Productos - Cuidado Personal',
        'meta_description' => 'Amplia cartera de productos para la industria cosmética',
        'meta_keywords' => 'Tripolifosfato, industria de cosmética, industria de cuidado personal',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">insumos</h1>
                <span class="subtitle-banner font-titles-md textUppercase">de alta calidad</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'src/includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-cuidado-personal"></i>
                                <h1 class="title-primary titles-big">CUIDADO</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">personal</h2>
                            </div>
                            <div class="img-products content-img-two d-none d-lg-block wow zoomIn">
                                <img src="assets/images/internas/bajas/cuidado-personal1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">productos</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico</li>
                                    <li class="item-subProducts font-titles-md">Ácidos funcionales</li>
                                    <li class="item-subProducts font-titles-md">Citrato de Trietilo</li>
                                    <li class="item-subProducts font-titles-md">Citrato de Tributilo</li>
                                    <li class="item-subProducts font-titles-md">Citrato Monosódico</li>
                                    <li class="item-subProducts font-titles-md">Citrato Tricálcico</li>
                                    <li class="item-subProducts font-titles-md">Citrato Trimagnesio</li>
                                    <li class="item-subProducts font-titles-md">Citrato Trisódico</li>
                                    <li class="item-subProducts font-titles-md">Colágeno</li>
                                    <li class="item-subProducts font-titles-md">Gluconato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Gluconato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Glucono-delta-lactona</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Lauril éter sulfato</li>
                                    <li class="item-subProducts font-titles-md">Polisorbatos</li>
                                    <li class="item-subProducts font-titles-md">Sales funcionales</li>
                                    <li class="item-subProducts font-titles-md">Zinc Citrato</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">productos</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Ácido Cítrico</li>
                                    <li class="item-subProducts font-titles-md">Ácidos funcionales</li>
                                    <li class="item-subProducts font-titles-md">Citrato de Trietilo</li>
                                    <li class="item-subProducts font-titles-md">Citrato de Tributilo</li>
                                    <li class="item-subProducts font-titles-md">Citrato Monosódico</li>
                                    <li class="item-subProducts font-titles-md">Citrato Tricálcico</li>
                                    <li class="item-subProducts font-titles-md">Citrato Trimagnesio</li>
                                    <li class="item-subProducts font-titles-md">Citrato Trisódico</li>
                                    <li class="item-subProducts font-titles-md">Colágeno</li>
                                    <li class="item-subProducts font-titles-md">Gluconato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Gluconato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Glucono-delta-lactona</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Potasio</li>
                                    <li class="item-subProducts font-titles-md">Lactato de Sodio</li>
                                    <li class="item-subProducts font-titles-md">Lauril éter sulfato</li>
                                    <li class="item-subProducts font-titles-md">Polisorbatos</li>
                                    <li class="item-subProducts font-titles-md">Sales funcionales</li>
                                    <li class="item-subProducts font-titles-md">Zinc Citrato</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="assets/images/icons/forma_esquina.svg" alt="">

        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>