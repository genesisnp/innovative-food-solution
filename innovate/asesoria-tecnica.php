<?php
    $data =  array(
        'pagetitle' => 'Servicios - Asesoría Técnica Especializada',
        'meta_description' => 'Asesoría Técnica en la aplicación de nuestros productos',
        'meta_keywords' => '',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="assets/images/banner/bajas/asesoria-tecnica.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">asesoría</h1>
                <span class="subtitle-banner font-titles-md textUppercase">TÉCNICA ESPECIALIZADA</span>
            </div>
        </section>
        <?php
            include 'src/includes/navbar-services-int.php'
        ?>
        <section class="sct-info-services overflow-h">
            <div class="container px-0">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-12 col-lg-11">
                                <div class="row justify-content-center">
                                    <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                        <i class="icon-descr icon-asesoria"></i>
                                        <h1 class="titles-big color-secondary">Asistencia</h1>
                                        <span class="span-desc font-internas textUppercase">OPTIMIZADA EN TODOS</span>
                                        <span class="subtitle-internas font-titles-md textUppercase">NUESTROS SERVICIOS</span>
                                    </div>
                                    <div class="col-12 col-lg-11 wow fadeInLeft">
                                        <p class="font-internas">Asesoría técnica en la aplicación de nuestros productos en los procesos de producción de nuestros
                                             clientes. para ello contamos con un equipo de especialistas de ingenieros con amplia experiencia.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-11 col-md-9 col-lg-6 px-0 wrapper-img-servDscrp">
                        <div class="content-img-servDscrp wow zoomIn">
                            <img src="assets/images/internas/bajas/asesoria-tecnica-desc.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="assets/images/icons/forma.svg" alt="">
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>