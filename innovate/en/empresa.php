<?php
    $data =  array(
        'pagetitle' => 'Empresa',
        'meta_description' => 'Empresa especializada en la importación de ingredientes , insumos y aditivos para la industria fundada en 2015, con sede principal en Madrid (España) y oficinas comerciales en los principales países latinoamericanos.',
        'meta_keywords' => 'Importacion ingredientes aditivos para alimentos, Importacion ingredientes aditivos para cosmetica, Importacion ingredientes aditivos para nutricion animal, Importacion ingredientes aditivos para industria farmaceutica, Importacion ingredientes aditivos para industria en general, Proveedor de tripolifosfato, Proveedor de maltodextrina, Proveedor de acido citrico, Proveedor de lacteos, Proveedor de aminoacidos, Proveedor de Vitaminas',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'includes/header.php'
?>
    <main class="main-company">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center wrapper-ancla">
            <img class="bg-shadow" src="../assets/images/sombra.png" alt="">
            <div class="content-img-banner vh">
                <img src="../assets/images/banner/bajas/Empresa.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big">COMMITMENT</h1>
                <span class="subtitle-banner font-titles-md">AND PROFESSIONALISM</span>
            </div>
            <a href="#inf-company" data-ancla="inf-company" class="content-ancla d-none d-lg-block">
                <h1 class="h1-text-Rotate font-internas color-white">More information</h1>
                <i class="color-white icon-flecha"></i>
            </a>
        </section>
        <!--DESCRIPTION EMPRESA-->
        <div class="content-bg-spinner overflow-h" id="inf-company">
            <section class="section-sct-description">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="row">
                                <div class="col-12 col-lg-11">
                                    <div class="row justify-content-center">
                                        <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                            <i class="icon-descr icon-suministro"></i>
                                            <h1 class="titles-big color-secondary">We supply</h1>
                                            <span class="span-desc font-internas">HIGH QUALITY</span>
                                            <span class="subtitle-internas font-titles-md">RAW MATERIALS</span>
                                        </div>
                                        <div class="col-12 col-xl-11 wow fadeInLeft">
                                            <p class="font-internas">INNOVATIVE FOOD SOLUTIONS S.L was created in 2015 with the objective to commercialize and distribute 
                                            chemical inputs for industries in general. Since that, we work to supply high quality raw materials and ingredients with a very good knowledge of the market, 
                                            a complete products offer and a professional team, all of this to help the customer in all the purchase process. Our commitment with the customer goes beyond of a commercial transaction, informing permantly to our customer so they can make the best purchase decision in the best moment.</p>
                                            <p class="font-internas">Our corporate structure, with headquarter in Madrid (Spain) and commercial offices in Southamerica and Asia, allow us to give the best service and keep 
                                            contact with the best input suppliers located in each continent.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="content-img wow zoomIn">
                                <img src="../assets/images/internas/bajas/empresa-int.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="sct-mv">
                <div class="container">
                    <div class="row">
                        <div class="col-12 content-mv">
                            <div class="row align-items-center">
                                <div class="col-12 col-lg-6 wrapper-img-mv wow zoomIn">
                                    <div class="container-img-mv">
                                        <img src="../assets/images/internas/bajas/mision.jpg" alt="img/vission">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5 wow fadeInRight">
                                    <h1 class="title-mv titles-big">Vision</h1>
                                    <p class="font-internas">To distribute chemical specialties that bring innovative solutions, 
                                    reliable and efficient giving the customer high quality products.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 content-mv">
                            <div class="row align-items-center justify-content-end">
                                <div class="col-12 col-lg-5 textR wow fadeInLeft">
                                    <h1 class="title-mv titles-big">Mission</h1>
                                    <p class="font-internas">To get the leadership in the national market, asia and america, as well as to bring integral solutions to our 
                                    customers, which can improve the food quality, health and environmental protection. </p>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="wrapper-img-mv W-img-mission wow zoomIn">
                                        <div class="container-img-mv container-img-mission">
                                            <img src="../assets/images/internas/bajas/vision.jpg" alt="img/mission">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <img class="forma-spinner fs-vss" src="../assets/images/icons/forma.svg" alt="">
                <img class="forma-spinner fs-mss" src="../assets/images/icons/forma.svg" alt="">
            </section>
            <img class="forma-spinner" src="../assets/images/icons/forma.svg" alt="">
        </div>
        
    </main>
    <?php
        include 'includes/footer.php'
    ?>
</body>

</html>