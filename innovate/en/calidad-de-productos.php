<?php
    $data =  array(
        'pagetitle' => 'Servicios - Calidad de Nuestros Productos',
        'meta_description' => 'Proveedores de productos con todas las garantías de calidad',
        'meta_keywords' => '',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'includes/header.php'
?>
    <main class="">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="../assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="../assets/images/banner/bajas/calidad_de_productos.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">QUALITY</h1>
                <span class="subtitle-banner font-titles-md textUppercase">OF OUR PRODUCTS</span>
            </div>
        </section>
        <?php
            include 'includes/navbar-services-int.php'
        ?>
        <section class="sct-info-services overflow-h">
            <div class="container px-0">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-12 col-lg-11">
                                <div class="row justify-content-center">
                                    <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                        <i class="icon-descr icon-calidad"></i>
                                        <h1 class="titles-big color-secondary">Specialized</h1>
                                        <span class="span-desc font-internas textUppercase">INFORMATION in</span>
                                        <span class="subtitle-internas font-titles-md textUppercase">OUR PRODUCTS</span>
                                    </div>
                                    <div class="col-12 col-lg-11 wow fadeInLeft">
                                        <p class="font-internas">Our wolrdwide suppliers network allow us to attend any product required by our customers. 
                                        Since technical specifications, certifications and sample homologations, until the physical visit of our technicals to the factories and/or suppliers to guarantee the product´s quality.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-11 col-md-9 col-lg-6 px-0 wrapper-img-servDscrp">
                        <div class="content-img-servDscrp wow zoomIn">
                            <img src="../assets/images/internas/bajas/calidad_de_productos_desc.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="../assets/images/icons/forma.svg" alt="">
        </section>
    </main>
    <?php
        include 'includes/footer.php'
    ?>
</body>

</html>