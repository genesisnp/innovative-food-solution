<?php
    $data =  array(
        'pagetitle' => 'Productos - Nutrición y Salud',
        'meta_description' => 'Amplia cartera de produtos para la industria de Nutrición Animal',
        'meta_keywords' => 'Goma Xantan, Ácido Láctico, Citratos',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'includes/header.php'
?>
    <main class="main-products">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="../assets/images/sombra.png" alt="">
            <div class="content-img-banner">
                <img src="../assets/images/banner/bajas/ind-alimentaria.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big textUppercase">insumos</h1>
                <span class="subtitle-banner font-titles-md textUppercase">de alta calidad</span>
            </div>
        </section>
        <!--NAVBAR PRODUCTOS-->
        <?php
            include 'includes/navbar-products.php'
        ?>
        <!--DESCRIPCION DE PRODUCTOS-->
        <section class="sct-description-products overflow-h">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 titles-big-products textR">
                                <i class="icon-desc-products icon-ind-alimentaria"></i>
                                <h1 class="title-primary titles-big">NUTRITION</h1>
                                <h2 class="title-secondary font-titles-md textUppercase">AND HEALTH</h2>
                            </div>
                            <div class="img-products content-img-one d-none d-lg-block wow zoomIn">
                                <img src="../assets/images/internas/bajas/nutricion-1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-lg-none content-accordion-mobile">
                        <ul class="accordion content-subProducts">
                            <li class="li-accordion"><a class="titles-subProducts titles-big">Essential fatty acids</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Alpha lipoic acid</li>
                                    <li class="item-subProducts font-titles-md">ARA</li>
                                    <li class="item-subProducts font-titles-md">DHA</li>
                                    <li class="item-subProducts font-titles-md">GABA</li>
                                </ul>
                            </li>
                            <li class="li-accordion"><a class="titles-subProducts titles-big">Aminoacids derivates</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">L-Carnitine</li>
                                    <li class="item-subProducts font-titles-md">L-Creatine</li>
                                    <li class="item-subProducts font-titles-md">L-Ornithine</li>
                                    <li class="item-subProducts font-titles-md">L-Theanine</li>
                                </ul>
                            </li>
                            <li class="li-accordion"><a class="titles-subProducts titles-big">Functional Vegetal Extracts</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aloe Vera</li>
                                    <li class="item-subProducts font-titles-md">Chitosan</li>
                                    <li class="item-subProducts font-titles-md">Chondroitin sulphate</li>
                                    <li class="item-subProducts font-titles-md">Echinacea</li>
                                    <li class="item-subProducts font-titles-md">Dong quai extract</li>
                                    <li class="item-subProducts font-titles-md">Ganoderma lucidum extract</li>
                                    <li class="item-subProducts font-titles-md">Ginkgo biloba</li>
                                    <li class="item-subProducts font-titles-md">Green tea/black tea extract</li>
                                    <li class="item-subProducts font-titles-md">Tribulis terrestris extract</li>
                                    <li class="item-subProducts font-titles-md">Garcinia Cambogia</li>
                                    <li class="item-subProducts font-titles-md">Glucosamine sulphate</li>
                                    <li class="item-subProducts font-titles-md">Soy isoflavones</li>
                                    <li class="item-subProducts font-titles-md">Cordyceps micelio</li>
                                    <li class="item-subProducts font-titles-md">Resveratrol 98%</li>
                                </ul>
                            </li>
                            <li class="li-accordion"><a class="titles-subProducts titles-big">Pharma</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Citric acid</li>
                                    <li class="item-subProducts font-titles-md">Sodium gluconate</li>
                                    <li class="item-subProducts font-titles-md">Gluconodeltalactone</li>
                                    <li class="item-subProducts font-titles-md">Zinc citrate</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Proteins and aminoacids</a>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparaginase</li>
                                    <li class="item-subProducts font-titles-md">Collagen</li>
                                    <li class="item-subProducts font-titles-md">L-Arginine</li>
                                    <li class="item-subProducts font-titles-md">L-Cysteine</li>
                                    <li class="item-subProducts font-titles-md">L-Glycine</li>
                                    <li class="item-subProducts font-titles-md">L-Glutamine</li>
                                    <li class="item-subProducts font-titles-md">L-Isoleucine</li>
                                    <li class="item-subProducts font-titles-md">L-Leucine</li>
                                    <li class="item-subProducts font-titles-md">L-Lisine</li>
                                    <li class="item-subProducts font-titles-md">L-Taurine</li>
                                    <li class="item-subProducts font-titles-md">L-Triptophan</li>
                                    <li class="item-subProducts font-titles-md">L-Valine</li>
                                    <li class="item-subProducts font-titles-md">N-Acetil Cysteine</li>
                                    <li class="item-subProducts font-titles-md">Isolated soy protein</li>
                                    <li class="item-subProducts font-titles-md">Actin</li>
                                    <li class="item-subProducts font-titles-md">Whey protein</li>
                                </ul>
                            </li>

                            <li class="li-accordion"><a class="titles-subProducts titles-big">Vitamins and minerals</a>
                                <ul class="list-subProducts">
                                    <!--<li class="item-subProducts font-titles-md">Bioferrin</li>-->
                                    <li class="item-subProducts font-titles-md">Tricalcium citrate</li>
                                    <li class="item-subProducts font-titles-md">Choline chloride</li>
                                    <!--<li class="item-subProducts font-titles-md">Complejo de Calcio lácteo</li>-->
                                    <li class="item-subProducts font-titles-md">Inositol</li>
                                    <li class="item-subProducts font-titles-md">Zinc oxid</li>
                                    <li class="item-subProducts font-titles-md">Vitamins A, B, C, etc</li>
                                    <li class="item-subProducts font-titles-md">Vitamin mixes</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-7 d-none d-lg-block">
                        <div class="wrapper_subproduct">
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Essential fatty acids</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Alpha lipoic acid</li>
                                    <li class="item-subProducts font-titles-md">ARA</li>
                                    <li class="item-subProducts font-titles-md">DHA</li>
                                    <li class="item-subProducts font-titles-md">GABA</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Aminoacids derivates</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">L-Carnitine</li>
                                    <li class="item-subProducts font-titles-md">L-Creatine</li>
                                    <li class="item-subProducts font-titles-md">L-Ornithine</li>
                                    <li class="item-subProducts font-titles-md">L-Theanine</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Functional Vegetal Extracts</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Aloe Vera</li>
                                    <li class="item-subProducts font-titles-md">Chitosan</li>
                                    <li class="item-subProducts font-titles-md">Chondroitin sulphate</li>
                                    <li class="item-subProducts font-titles-md">Echinacea</li>
                                    <li class="item-subProducts font-titles-md">Dong quai extract</li>
                                    <li class="item-subProducts font-titles-md">Ganoderma lucidum extract</li>
                                    <li class="item-subProducts font-titles-md">Ginkgo biloba</li>
                                    <li class="item-subProducts font-titles-md">Green tea/black tea extract</li>
                                    <li class="item-subProducts font-titles-md">Tribulis terrestris extract</li>
                                    <li class="item-subProducts font-titles-md">Garcinia Cambogia</li>
                                    <li class="item-subProducts font-titles-md">Glucosamine sulphate</li>
                                    <li class="item-subProducts font-titles-md">Soy isoflavones</li>
                                    <li class="item-subProducts font-titles-md">Cordyceps micelio</li>
                                    <li class="item-subProducts font-titles-md">Resveratrol 98%</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Pharma</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Citric acid</li>
                                    <li class="item-subProducts font-titles-md">Sodium gluconate</li>
                                    <li class="item-subProducts font-titles-md">Gluconodeltalactone</li>
                                    <li class="item-subProducts font-titles-md">Zinc citrate</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Proteins and aminoacids</h1>
                                <ul class="list-subProducts">
                                    <li class="item-subProducts font-titles-md">Asparaginase</li>
                                    <li class="item-subProducts font-titles-md">Collagen</li>
                                    <li class="item-subProducts font-titles-md">L-Arginine</li>
                                    <li class="item-subProducts font-titles-md">L-Cysteine</li>
                                    <li class="item-subProducts font-titles-md">L-Glycine</li>
                                    <li class="item-subProducts font-titles-md">L-Glutamine</li>
                                    <li class="item-subProducts font-titles-md">L-Isoleucine</li>
                                    <li class="item-subProducts font-titles-md">L-Leucine</li>
                                    <li class="item-subProducts font-titles-md">L-Lisine</li>
                                    <li class="item-subProducts font-titles-md">L-Taurine</li>
                                    <li class="item-subProducts font-titles-md">L-Triptophan</li>
                                    <li class="item-subProducts font-titles-md">L-Valine</li>
                                    <li class="item-subProducts font-titles-md">N-Acetil Cysteine</li>
                                    <li class="item-subProducts font-titles-md">Isolated soy protein</li>
                                    <li class="item-subProducts font-titles-md">Actin</li>
                                    <li class="item-subProducts font-titles-md">Whey protein</li>
                                </ul>
                            </div>
                            <div class="content-subProducts">
                                <h1 class="titles-subProducts titles-big">Vitamins and minerals</h1>
                                <ul class="list-subProducts">
                                    <!--<li class="item-subProducts font-titles-md">Bioferrin</li>-->
                                    <li class="item-subProducts font-titles-md">Tricalcium citrate</li>
                                    <li class="item-subProducts font-titles-md">Choline chloride</li>
                                    <!--<li class="item-subProducts font-titles-md">Complejo de Calcio lácteo</li>-->
                                    <li class="item-subProducts font-titles-md">Inositol</li>
                                    <li class="item-subProducts font-titles-md">Zinc oxid</li>
                                    <li class="item-subProducts font-titles-md">Vitamins A, B, C, etc</li>
                                    <li class="item-subProducts font-titles-md">Vitamin mixes</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img class="forma-spinner"src="../assets/images/icons/forma.svg" alt="">
            <img class="forma-esquina" src="../assets/images/icons/forma_esquina.svg" alt="">
        </section>
    </main>
    <?php
        include 'includes/footer.php'
    ?>
    <script src="../assets/js/accordion.js"></script>
</body>

</html>