<section class="sct-navbar-products nb-serv">
    <div class="wow zoomIn wrapper-navP <?= in_array('garantizamos-el-despacho.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="garantizamos-el-despacho.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-despacho"></i>
                <h1 class="font-titles-md textUppercase">WE GUARANTEE<br>THE DELIVERY</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('asesoria-tecnica.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="asesoria-tecnica.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-asesoria"></i>
                <h1 class="font-titles-md textUppercase">SPECIALISED TECHNICAL<br>CONSULTING</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('calidad-de-productos.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="calidad-de-productos.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-calidad"></i>
                <h1 class="font-titles-md textUppercase">QUALITY<br>OF OUR<br>PRODUCTS</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('condiciones-de-pago.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="condiciones-de-pago.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cond-de-pago"></i>
                <h1 class="font-titles-md textUppercase">FAVORABLE<br>PAYMENT<br>CONDITIONS</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('informacion-del-mercado.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="informacion-del-mercado.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-info-de-mercad"></i>
                <h1 class="font-titles-md textUppercase">CURRENT MARKET<br>INFORMATION</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('informacion-de-tendencias.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="informacion-de-tendencias.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-info-de-tend"></i>
                <h1 class="font-titles-md textUppercase">TENDENCY<br>INFORMATION</h1>
            </div>
        </a>
    </div>
</section>