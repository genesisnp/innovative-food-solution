<footer class="footer bg-black">
    <div class="container">
        <div class="row justify-content-between">
            <div class="mb-w100">
                <p class="color-white text-credits text-company">INNOVATIVE FOOD SOLUTIONS</p>
                <p class="color-white text-credits oculMob">TODOS LOS DERECHOS RESERVADOS 2019</p>
            </div>
            <div class="d-flex marB-1">
                <i class="icon-footer icon-ubicacion"></i>
                <div>
                    <p class="text-f font-titles-md">Calle Lagasca N° 5, 1° Izquierda<br>Madrid 28001, España</p>
                </div>
            </div>
            <div class="d-flex marB-1">
                <i class="icon-footer icon-phone-f"></i>
                <div class="d-flex flex-column font-titles-md">
                    <a href="tel:+34915799360" class="text-f mb">+34 915 799 360</a>
                    <a href="tel:+34665324557" class="text-f mb">+34 665 324 557</a>
                </div>
            </div>
            <div class="d-flex marB-1">
                <i class="icon-footer icon-email"></i>
                <div class="d-flex flex-column font-titles-md">
                    <a href="mailto:exports@innovativefoodsolutions.es" class="text-f mb">exports@innovativefoodsolutions.es</a>
                    <a href="mailto:logistics@innovativefoodsolutions.es" class="text-f mb">logistics@innovativefoodsolutions.es</a>
                </div>
            </div>
            <div class="crd">
                <p class="color-white text-credits dres-mob">TODOS LOS DERECHOS RESERVADOS 2019</p>
                <p><span class="color-white text-credits">POWERED BY </span><a href="http://exe.pe/" class="color-white text-credits">EXE.DIGITAL</a></p>
                <p class="oculMob"><a href="https://validator.w3.org/check?uri=referer" class="text-f text-credits">HTML </a><a href="https://jigsaw.w3.org/css-validator/check/referer" class="text-f text-credits">CSS</a></p>

            </div>
        </div>
    </div>
</footer>
<script src="../assets/js/app.js"></script>
<script src="../assets/js/libraries/wow.min.js"></script>
<script>
    var wow = new WOW({
        boxClass:     'wow',    
        animateClass: 'animated', 
        offset:       0,          
        mobile:       false,   
        live:         true,    
        callback:     function(box) {
        },
            scrollContainer: null,  
            resetAnimation: true,  
        }
    );
    wow.init();
</script>

