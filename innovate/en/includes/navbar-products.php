<section class="sct-navbar-products">
    <div class="wow zoomIn wrapper-navP <?= in_array('industria-alimentaria.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="industria-alimentaria.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-ind-alimentaria"></i>
                <h1 class="font-titles-md textUppercase">FOOD<br>INDUSTRY</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('nutricion-y-salud.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="nutricion-y-salud.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-nutricion"></i>
                <h1 class="font-titles-md textUppercase">NUTRITION<br>AND HEALTHY</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('cuidado-personal.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="cuidado-personal.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cuidado-personal"></i>
                <h1 class="font-titles-md textUppercase">PERSONAL<br>CARE</h1>
            </div>
        </a>
    </div>
    <div class="wow zoomIn wrapper-navP <?= in_array('cuidado-del-hogar.php', $uriSegments ) ? 'active' : ''; ?>">
        <img class="img-spinner" src="../assets/images/spiner.png" alt="">
        <a href="cuidado-del-hogar.php" class="contenedor-navP">
            <div class="info-nav">
                <i class="icon-nP icon-cuidado-hogar"></i>
                <h1 class="font-titles-md textUppercase">HOME CARE<br>AND OTHERS</h1>
            </div>
        </a>
    </div>
</section>