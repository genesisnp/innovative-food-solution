<form action="#" class="form row" method="post" id="form-contact">
    <div class="form__wrapper col-12">
        <input type="text" class="form__input" id="nameAndLastname" name="nameAndLastname">
        <label class="form__label">
            <span class="form__label-content">Name and Surname</span>
        </label>
    </div>
    <div class="form__wrapper col-12 col-lg-6">
        <input type="text" class="form__input" id="company" name="company">
        <label class="form__label">
            <span class="form__label-content">Company</span>
        </label>
    </div>
    <div class="form__wrapper col-12 col-lg-6">
        <input type="email" class="form__input" id="email" name="email">
        <label class="form__label">
            <span class="form__label-content">Email</span>
        </label>
    </div>
    <div class="form__wrapper col-12 col-lg-6">
        <input type="text" class="form__input" id="phone"  name="phone">
        <label class="form__label">
            <span class="form__label-content">Phone</span>
        </label>
    </div>
    <div class="form__wrapper col-12 col-sm-6 col-lg-6 d-flex align-items-center">
        <div class="input_file d-flex">
            <div class="img-file d-flex justify-content-center align-items-center"><img
                    src="../assets/images/icons/archivo.svg" alt=""></div>
            <label class="file_label d-flex align-items-center font-titles-md">
                <span class="font-titles-reg">Add File</span>
            </label>
            <input id="file" type="file" class="font-titles-md" name="file" multiple />
        </div>
    </div>
    <div class="form__wrapper col-12">
        <textarea class="form_textarea" id="textarea" name="textarea"></textarea>
        <label class="form__label">
            <span class="form__label-content">Mensaje</span>
        </label>
    </div>
    <div class="d-flex justify-content-between align-items-center col-12 flex-column flex-lg-row">
        <div class="checkbox">
            <label class="font-titles-md label-pol">
                <input type="checkbox" /><i class="helper"></i><span>You confirm to have read accepted <span class="span-pol color-primary btn-modals btn-modals1">the personal data policy.</span></span>
            </label>
        </div>
        <div class="btn-container">
            <button type="submit" name="submit" class="btn font-titles-md btn-send btn-modals btn-modals2" id="btn-send-form">SEND</button>
        </div>
    </div>
</form>