<?php
    include 'src/includes/header.php'
?>
    <main class="main-contactUs overflow-h">
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner vh">
                <img src="assets/images/banner/bajas/contacto.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big">Contacto</h1>
                <span class="subtitle-banner font-titles-md">a nivel global</span>
            </div>
        </section>
        <div class="bg-spinner-contacUs overflow-h">
            <img class="forma-spinner" src="assets/images/icons/forma.svg" alt="">
            <?php
                include 'src/includes/contacto.php'
            ?>
            <img class="forma-spinner spinner-formLeft" src="assets/images/icons/forma.svg" alt="">
        </div>
        

    </main>
    
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/modal-pol-dat-confi.js"></script>
    <script src="assets/js/modal-pol-dat.js"></script>
    <script src="assets/js/input-file.js"></script>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form.js"></script>
</body>


</html>
