<?php
    $data =  array(
        'pagetitle' => 'Empresa',
        'meta_description' => 'Empresa especializada en la importación de ingredientes , insumos y aditivos para la industria fundada en 2015, con sede principal en Madrid (España) y oficinas comerciales en los principales países latinoamericanos.',
        'meta_keywords' => 'Importacion ingredientes aditivos para alimentos, Importacion ingredientes aditivos para cosmetica, Importacion ingredientes aditivos para nutricion animal, Importacion ingredientes aditivos para industria farmaceutica, Importacion ingredientes aditivos para industria en general, Proveedor de tripolifosfato, Proveedor de maltodextrina, Proveedor de acido citrico, Proveedor de lacteos, Proveedor de aminoacidos, Proveedor de Vitaminas',
        'og_title' => 'og_title',
        'og_description' => 'og_description',
        'og_image' => 'og_imagen',
    );
    include 'src/includes/header.php'
?>
    <main class="main-company">
        <!--BANNER-->
        <section class="sct-banner container-fluid px-0 d-flex justify-content-center wrapper-ancla">
            <img class="bg-shadow" src="assets/images/sombra.png" alt="">
            <div class="content-img-banner vh">
                <img src="assets/images/banner/bajas/Empresa.jpg" alt="">
            </div>
            <div class="content-text-banner container d-flex flex-column justify-content-end wow fadeInLeft">
                <h1 class="title-big-banner titles-big">COMPROMISO</h1>
                <span class="subtitle-banner font-titles-md">Y PROFESIONALISMO</span>
            </div>
            <a href="#inf-company" data-ancla="inf-company" class="content-ancla d-none d-lg-block">
                <h1 class="h1-text-Rotate font-internas color-white">Más información</h1>
                <i class="color-white icon-flecha"></i>
            </a>
        </section>
        <!--DESCRIPTION EMPRESA-->
        <div class="content-bg-spinner overflow-h" id="inf-company">
            <section class="section-sct-description">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="row">
                                <div class="col-12 col-lg-11">
                                    <div class="row justify-content-center">
                                        <div class="col-12 content-description px-0 d-flex flex-column wow fadeInLeft">
                                            <i class="icon-descr icon-suministro"></i>
                                            <h1 class="titles-big color-secondary">Suministramos</h1>
                                            <span class="span-desc font-internas">MATERIA PRIMA</span>
                                            <span class="subtitle-internas font-titles-md">DE ALTA
                                                CALIDAD</span>
                                        </div>
                                        <div class="col-12 col-xl-11 wow fadeInLeft">
                                            <p class="font-internas">Innovative Food Solutions fue fundada en 2015 con el
                                                objetivo de comercializar y distribuir aditivos químicos para la industria
                                                en general.
                                                Desde entonces, trabajamos para suministrar materias primas e ingredientes
                                                de alta calidad partiendo de un profundo conocimiento del mercado, una
                                                completa oferta de productos y un equipo de profesionales que acompañar a
                                                nuestros clientes en todas en las fases del proceso de compra. Nuestro
                                                compromiso con el cliente va más allá de una transacción comercial,
                                                informando permanentemente a nuestros clientes para que puedan tomar la
                                                mejor decisión de compra en el momento oportuno.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="content-img wow zoomIn">
                                <img src="assets/images/internas/bajas/empresa-int.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="sct-mv">
                <div class="container">
                    <div class="row">
                        <div class="col-12 content-mv">
                            <div class="row align-items-center">
                                <div class="col-12 col-lg-6 wrapper-img-mv wow zoomIn">
                                    <div class="container-img-mv">
                                        <img src="assets/images/internas/bajas/mision.jpg" alt="img/vission">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5 wow fadeInRight">
                                    <h1 class="title-mv titles-big">Visión</h1>
                                    <p class="font-internas">Nuestra visión es distribuir especialidades químicas que
                                        aporten soluciones
                                        innovadoras, fiables y eficientes poniendo a disposición de los clientes productos
                                        de alta calidad.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 content-mv">
                            <div class="row align-items-center justify-content-end">
                                <div class="col-12 col-lg-5 textR wow fadeInLeft">
                                    <h1 class="title-mv titles-big">Misión</h1>
                                    <p class="font-internas">Nuestra misión es conseguir el liderazgo en el mercado
                                        nacional, Asia y América, así
                                        como brindar soluciones integrales a nuestros clientes, las cuales contribuyan a
                                        mejorar la calidad alimenticia, la salud de las personas y el cuidado del medio
                                        ambiente.</p>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="wrapper-img-mv W-img-mission wow zoomIn">
                                        <div class="container-img-mv container-img-mission">
                                            <img src="assets/images/internas/bajas/vision.jpg" alt="img/mission">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <img class="forma-spinner fs-vss" src="assets/images/icons/forma.svg" alt="">
                <img class="forma-spinner fs-mss" src="assets/images/icons/forma.svg" alt="">
            </section>
            <img class="forma-spinner" src="assets/images/icons/forma.svg" alt="">
        </div>
        
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>