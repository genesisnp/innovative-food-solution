const btnModal = document.querySelectorAll('.btn-modals2');
const modal = document.querySelector('.modal-content2');
const modalCloseBtn = document.querySelector('#modal-close-btn');
const sctModal = document.querySelector('.sct-modal2');

btnModal.forEach((btnModals) => {
    btnModals.addEventListener('click', (e) => {
        modal.style.display = 'flex';
    })
})

modalCloseBtn.addEventListener('click', (e) => {
    modal.style.display = 'none';
});

modal.addEventListener('click', (e) => {
    if(e.target.classList.contains('modal-container')) {
        modal.style.display = 'none';
    }
});
sctModal.addEventListener('click', (e)=> {
    modal.style.display = 'none';
})

