<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INNOVATIVE FOOD SOLUTIONS - <?php echo $data['pagetitle'] ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo-ifs.ico" />
    <meta name="description" content="<?php echo $data['meta_description'] ?>">
    <meta name="keywords" content="<?php echo $data['meta_keywords'] ?>">
    <meta name="author" content="EXE MARKETING DIGITAL INTEGRADO" />
    <meta name="copyright" content="INNOVATIVE FOOD SOLUTIONS" />
    <meta name="language" content="ES" />
    <meta name="robots" content="index,follow" />
    <link rel="stylesheet" href="assets/css/app.css">
</head>

<body>
    <?php 
        $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));    
    ?>
    <header class="header">
        <img class="bg-logo" src="assets/images/bg-logo.png" alt="bg/logo">
        <div class="logo">
            <a href="index.php">
                <img class="img-logo" src="assets/images/logo-innovative.png" alt="logo/innovative">
            </a>
        </div>
        <i id="button-menu" class="icon-hamburger"></i>

        <div class="inner-header container">
            <div class="content-menu floatR">
                <div class="phonesHeader textR">
                    <i class="icon-phone-h"></i>
                    <a class="aPhone aPhoneOne font-titles-md" href="tel:+34648887963"><span>+34 648 887 963</span></a>
                    <a class="aPhone font-titles-md" href="tel:+34915789360"><span>+34 915 789 360</span></a>
                    <a href="en/index.php" class="aPhone lenguage font-titles-md color-white"><span>EN</span></a>
                </div>
                <nav class="navbar">
                    <ul class="navbarList floatR">
                        <li class="navbarItem  <?= in_array('empresa.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="empresa.php">empresa</a></li>
                        <li class="navbarItem  <?= (in_array('industria-alimentaria.php', $uriSegments )
                                                    or in_array('nutricion-y-salud.php', $uriSegments )
                                                    or in_array('cuidado-personal.php', $uriSegments )
                                                    or in_array('cuidado-del-hogar.php', $uriSegments ))? 'active' : ''; ?>">
                            <a class="navbarLink font-titles-md" href="#">productos</a>
                            <div class="submenu">
                                <div class="wrapper-submenu">
                                    <ul class="tabs floatL container">
                                        <li class="tab-item  <?= in_array('industria-alimentaria.php', $uriSegments ) ? 'active' : ''; ?>"><a class="tab-link textUppercase font-titles-md"
                                                href="industria-alimentaria.php" data-name="#tab1">Industria Alimentaria</a>
                                        </li>
                                        <li class="tab-item  <?= in_array('cuidado-personal.php', $uriSegments ) ? 'active' : ''; ?>"><a class="tab-link textUppercase font-titles-md" href="cuidado-personal.php"
                                                data-name="#tab2">Cuidado Personal</a></li>
                                        <li class="tab-item  <?= in_array('nutricion-y-salud.php', $uriSegments ) ? 'active' : ''; ?>"><a class="tab-link textUppercase font-titles-md" href="nutricion-y-salud.php"
                                                data-name="#tab3">Nutrición y salud</a></li>
                                        <li class="tab-item  <?= in_array('cuidado-del-hogar.php', $uriSegments ) ? 'active' : ''; ?>"><a class="tab-link textUppercase font-titles-md" href="cuidado-del-hogar.php"
                                                data-name="#tab4">cuidado del hogar y otros</a></li>
                                    </ul>

                                    <div class="secciones floatL">
                                        <div class="tab-img" id="tab1">
                                            <img class="wow fadeIn" src="assets/images/internas/bajas/ind-alimentaria-int1.jpg" alt="">
                                        </div>
                                        <div class="tab-img" id="tab2">
                                            <img class="wow fadeIn" src="assets/images/internas/bajas/cuidado-personal1.jpg" alt="">
                                        </div>
                                        <div class="tab-img" id="tab3">
                                            <img class="wow fadeIn" src="assets/images/internas/bajas/nutricion-1.jpg" alt="">
                                        </div>
                                        <div class="tab-img" id="tab4">
                                            <img class="wow fadeIn" src="assets/images/internas/bajas/limpieza.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="navbarItem  <?= (in_array('garantizamos-el-despacho.php', $uriSegments ) 
                                                    or in_array('asesoria-tecnica.php', $uriSegments )
                                                    or in_array('calidad-de-productos.php', $uriSegments )
                                                    or in_array('condiciones-de-pago.php', $uriSegments )
                                                    or in_array('informacion-de-mercado.php', $uriSegments )
                                                    or in_array('informacion-de-tendencias.php', $uriSegments )) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="garantizamos-el-despacho.php">servicios</a></li>
                        <li class="navbarItem  <?= in_array('clientes.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="clientes.php">clientes</a>
                        </li>
                        <li class="navbarItem  <?= in_array('contacto.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink font-titles-md" href="contacto.php">contacto</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>